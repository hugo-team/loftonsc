<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['aviso-de-privacidad.html'] = 'home/privacy';
// Servicios // 
$route['servicios.html'] = 'servicios';
//============================================================================//
//Contabilidad 
$route['servicios/contabilidad.html'] = 'servicios/service/contabilidad';
$route['servicios/contabilidad/contabilidad-general.html'] = 'servicios/sub_service/contabilidad_general' ;
$route['servicios/contabilidad/asesoria-financiera.html'] = 'servicios/sub_service/asesoria_financiera';
$route['servicios/contabilidad/asesoria-fiscal.html'] = 'servicios/sub_service/asesoria_fiscal';
$route['servicios/contabilidad/inventarios.html'] = 'servicios/sub_service/inventarios';

//Auditoría
$route['servicios/auditoria.html'] = 'servicios/service/auditoria';
$route['servicios/auditoria/auditoria-fiscal.html'] = 'servicios/sub_service/auditoria_fiscal' ;
$route['servicios/auditoria/auditoria-financiera.html'] = 'servicios/sub_service/auditoria_financiera' ;
$route['servicios/auditoria/auditoria-ante-imss-e-infonavit.html'] = 'servicios/sub_service/auditoria_ante_imss_e_infonavit' ;
$route['servicios/auditoria/auditoria-en-contribuciones-locales.html'] = 'servicios/sub_service/auditoria_en_contribuciones_locales' ;
$route['servicios/auditoria/auditoria-operativa.html'] = 'servicios/sub_service/auditoria_operativa';
$route['servicios/auditoria/trabajos-especiales.html'] = 'servicios/sub_service/trabajos_especiales';

$route['servicios/auditoria/trabajos-especiales/dictamen-enajenacion.html'] = 'servicios/sub_sub_service/dictamen_enajenacion' ;
$route['servicios/auditoria/trabajos-especiales/devolucion-saldo.html'] = 'servicios/sub_sub_service/devolucion_saldo';
$route['servicios/auditoria/trabajos-especiales/revision-rubros.html'] = 'servicios/sub_sub_service/revision_rubros';
$route['servicios/auditoria/trabajos-especiales/auditoria-forense.html'] = 'servicios/sub_sub_service/auditoria_forense';

//Nóminas
$route['servicios/nominas.html'] = 'servicios/service/nominas';
$route['servicios/nominas/administracion-de-nomina.html'] = 'servicios/sub_service/administracion_de_nomina';
$route['servicios/nominas/administracion-de-seguro-social.html'] = 'servicios/sub_service/administracion_de_seguro_social';
$route['servicios/nominas/maquila-de-nomina.html'] = 'servicios/sub_service/maquila_de_nomina';

//Jurídico 
$route['servicios/juridico.html'] = 'servicios/service/juridico';
$route['servicios/juridico/juridico-civil.html'] = 'servicios/sub_service/juridico_civil';
$route['servicios/juridico/juridico-corporativo.html'] = 'servicios/sub_service/juridico_corporativo';
$route['servicios/juridico/juridico-familiar.html'] = 'servicios/sub_service/juridico_familiar';
$route['servicios/juridico/juridico-fiscal.html'] = 'servicios/sub_service/juridico_fiscal';
$route['servicios/juridico/juridico-laboral.html'] = 'servicios/sub_service/juridico_laboral';
$route['servicios/juridico/juridico-mercantil.html'] = 'servicios/sub_service/juridico_mercantil';

$route['servicios/juridico/juridico-civil/juicios-de-arrendamiento.html'] = 'servicios/sub_sub_service/juicios_de_arrendamiento';
$route['servicios/juridico/juridico-civil/contratos-civiles.html'] = 'servicios/sub_sub_service/contratos_civiles';
$route['servicios/juridico/juridico-civil/litigio-civil-y-mercantil.html'] = 'servicios/sub_sub_service/litigio_civil_y_mercantil';
$route['servicios/juridico/juridico-civil/especial-hipotecario.html'] = 'servicios/sub_sub_service/especial_hipotecario';
$route['servicios/juridico/juridico-civil/juicios-civiles.html'] = 'servicios/sub_sub_service/juicios_civiles';

$route['servicios/juridico/juridico-corporativo/derecho-corporativo.html'] = 'servicios/sub_sub_service/derecho_corporativo';
$route['servicios/juridico/juridico-corporativo/propiedad-industrial.html'] = 'servicios/sub_sub_service/propiedad_industrial';
$route['servicios/juridico/juridico-corporativo/derecho-migratorio.html'] = 'servicios/sub_sub_service/derecho_migratorio';
$route['servicios/juridico/juridico-corporativo/gestoria-de-negocios.html'] = 'servicios/sub_sub_service/gestoria_de_negocios';

$route['servicios/juridico/juridico-familiar/divorcios.html'] = 'servicios/sub_sub_service/divorcios';
$route['servicios/juridico/juridico-familiar/controversia-familiar.html'] = 'servicios/sub_sub_service/controversia_familiar';
$route['servicios/juridico/juridico-familiar/interdiccion.html'] = 'servicios/sub_sub_service/interdiccion';
$route['servicios/juridico/juridico-familiar/herencia.html'] = 'servicios/sub_sub_service/herencia';
$route['servicios/juridico/juridico-familiar/reconocimiento-de-paternidad.html'] = 'servicios/sub_sub_service/reconocimiento_de_paternidad';

$route['servicios/juridico/juridico-fiscal/juicio-contencioso.html'] = 'servicios/sub_sub_service/juicio_contencioso';
$route['servicios/juridico/juridico-fiscal/recursos-de-revocacion-e-inconformidad.html'] = 'servicios/sub_sub_service/recursos_de_revocacion_e_inconformidad';
$route['servicios/juridico/juridico-fiscal/amparo-fiscal.html'] = 'servicios/sub_sub_service/amparo-fiscal';
$route['servicios/juridico/juridico-fiscal/gestion-fiscal-administrativa.html'] = 'servicios/sub_sub_service/gestion_fiscal_administrativa';
$route['servicios/juridico/juridico-fiscal/acuerdos-conclusivos.html'] = 'servicios/sub_sub_service/acuerdos_conclusivos';
$route['servicios/juridico/juridico-fiscal/quejas-ante-la-prodecon.html'] = 'servicios/sub_sub_service/quejas_ante_la_prodecon';
$route['servicios/juridico/juridico-fiscal/procedimientos-de-verificacion.html'] = 'servicios/sub_sub_service/procedimientos_de_verificacion';

$route['servicios/juridico/juridico-laboral/auditoria-laboral.html'] = 'servicios/sub_sub_service/auditoria_laboral';
$route['servicios/juridico/juridico-laboral/despido-justificado.html'] = 'servicios/sub_sub_service/despido_justificado';
$route['servicios/juridico/juridico-laboral/diligencias.html'] = 'servicios/sub_sub_service/diligencias';
$route['servicios/juridico/juridico-laboral/representacion-juridica.html'] = 'servicios/sub_sub_service/representacion_juridica';

$route['servicios/juridico/juridico-mercantil/cobranza.html'] = 'servicios/sub_sub_service/cobranza';
$route['servicios/juridico/juridico-mercantil/juicios-mercantiles.html'] = 'servicios/sub_sub_service/juicios_mercantiles';
$route['servicios/juridico/juridico-mercantil/contratos-mercantiles.html'] = 'servicios/sub_sub_service/contratos_mercantiles';

//Consultoría de Negocios

$route['servicios/consultoria-de-negocios.html'] = 'servicios/service/consultoria_de_negocios';
$route['servicios/consultoria-de-negocios/reingenieria-de-procesos.html'] = 'servicios/sub_service/reingenieria_de_procesos';
$route['servicios/consultoria-de-negocios/reingenieria-organizacional.html'] = 'servicios/sub_service/reingenieria_organizacional';
$route['servicios/consultoria-de-negocios/erps-sistemas-de-informacion.html'] = 'servicios/sub_service/erps_sistemas_de_informacion';
$route['servicios/consultoria-de-negocios/cambio-estrategico.html'] = 'servicios/sub_service/cambio_etrategico';
$route['servicios/consultoria-de-negocios/cadena-de-suministro.html'] = 'servicios/sub_service/cadena_de_suministro';

//Recursos Humanos
$route['servicios/recursos-humanos.html'] = 'servicios/service/recursos_humanos';
$route['servicios/recursos-humanos/atraccion-de-talento.html'] = 'servicios/sub_service/atraccion_de_talento';
$route['servicios/recursos-humanos/estudios-sociolaborales.html'] = 'servicios/sub_service/estudios_sociolaborales';
$route['servicios/recursos-humanos/evaluacion-psicometrica-por-competencia.html'] = 'servicios/sub_service/evaluacion_psicometrica_por_competencia';

//Tecnologías de la Información
$route['servicios/tecnologias-de-la-informacion.html'] = 'servicios/service/tecnologias_de_la_informacion';
$route['servicios/tecnologias-de-la-informacion/hosting.html'] = 'servicios/sub_service/hosting';
$route['servicios/tecnologias-de-la-informacion/servicios-de-ti.html'] = 'servicios/sub_service/servicios_de_ti';
$route['servicios/tecnologias-de-la-informacion/servicios-de-consultoria.html'] = 'servicios/sub_service/servicios_de_consultoria';
$route['servicios/tecnologias-de-la-informacion/help-desk.html'] = 'servicios/sub_service/help_desk';
$route['servicios/tecnologias-de-la-informacion/infraestructura.html'] = 'servicios/sub_service/infraestructura';

//Consultoría en Mercadotecnia
$route['servicios/consultoria-en-mercadotecnia.html'] = 'servicios/service/consultoria_en_mercadotecnia';
$route['servicios/consultoria-en-mercadotecnia/marketing_digital.html'] = 'servicios/sub_service/marketing_digital';
$route['servicios/consultoria-en-mercadotecnia/diseno.html'] = 'servicios/sub_service/diseno';
$route['servicios/consultoria-en-mercadotecnia/produccion_edicion_video.html'] = 'servicios/sub_service/produccion_edicion_video';

//Fianzas y Seguros
$route['servicios/fianzas-y-seguros.html'] = 'servicios/service/finanzas_y_seguros';

//Fin Servicios//
//============================================================================//


//Acerca de Lofton//
$route['acerca-de-lofton/conocenos.html'] = 'about/conocenos';
$route['acerca-de-lofton/historia.html'] = 'about/historia';
$route['acerca-de-lofton/por-que-lofton.html'] = 'about/porque_lofton';
$route['acerca-de-lofton/problemas-dificiles.html'] = 'about/problemas_dificiles';

//CARRERA
$route['carrera.html'] = 'career';
$route['carrera/plan-de-carrera.html'] = 'career/plan_de_carrera';
$route['carrera/testimoniales.html'] = 'career/testimoniales';
$route['carrera/vacantes.html'] = 'career/vacantes';
$route['carrera/sube-tu-cv.html'] = 'career/curriculum';
$route['carrera/enlacelofton'] = 'career/enlace_lofton';


//CONTACTO
$route['contacto.html'] = 'contact';
