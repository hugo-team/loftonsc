<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database('blog');
    }

    private $postLimit = 8;
    private $imagesPerPost = 1;

    public function getPosts() {
        $this->db->where('wp_posts.post_parent', 0);
        $this->db->where('wp_posts.post_status', 'publish');
        $this->db->limit($this->postLimit);
        $this->db->order_by('post_date', 'DESC');
        $query = $this->db->get('wp_posts');
        $data = $query->result();
        $post = array();
        for ($i = 0; $i < count($data); $i++) {
            array_push($post, array(
                'post' => $data[$i],
                'image' => $this->getPostImages($data[$i]->ID)
                    )
            );
        }

        //echo "<PRE>"; var_dump($post); echo "</PRE>";

        return $post;
    }
    private function getPostImages($idPost) {
        $this->db->where('post_type', 'attachment');
        $this->db->where('post_parent', $idPost);

        $this->db->limit($this->imagesPerPost);
        $this->db->order_by('post_date', 'DESC');

        $image = $this->db->get('wp_posts');

        if ($this->imagesPerPost > 1) {
            return $image->result();
        } else {
            return $image->row();
        }
    }
}
