<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Servicios_model extends CI_Model {

    function Servicios_model() {
        //parent::__construct(); //llamada al constructor de Model.
        $this->load->database();
    }
    
    function getServiceData($service) {
        $this->db->select('*');
        $this->db->from('servicio');
        if ($service != null){
        $this->db->where('clave_servicio', $service);
        }
        $result= $this->db->get();                
        return $result->result();
        
    }
    
    function getSubServices($id_service,$condition) {   
   
        $this->db->select('clave_subservicio, subservicio, descripcion_breve, img_subservicio, link_detalle');
        $this->db->from('subservicio');
        if($condition == NULL){
          $this->db->where('id_servicio', $id_service);  
        }else{
            $where = array('id_servicio' => $id_service, 'clave_subservicio <> ' => $condition);
            $this->db->where( $where); 
        }        
        $result= $this->db->get();                
        return $result->result();
        
    }
    
    function getSubSubServices($id_subService,$condition) {   
   
        $this->db->select('clave, sub_subservicio, descripcion_breve, img_subservicio, link_detalle');
        $this->db->from('sub_subservicio');
        if($condition == NULL){
          $this->db->where('id_subservicio', $id_subService);  
        }else{
            $where = array('id_subservicio' => $id_subService, 'clave <> ' => $condition);
            $this->db->where( $where); 
        }        
        $result= $this->db->get();                
        return $result->result();
        
    }
       
    
    function getManager($id_service) {       
        $this->db->select('nombre,puesto');
        $this->db->from('gerentes');
        $this->db->where('area', $id_service);
        $result= $this->db->get();                
        return $result->result();
        
    }
    
    function getSubServiceData($subService) {
        $this->db->select('*');
        $this->db->from('subservicio');
        $this->db->where('clave_subservicio', $subService);
        $result= $this->db->get();                
        return $result->result();
        
    }
    
    function getSubSubServiceData($subSubService) {
        $this->db->select('*');
        $this->db->from('sub_subservicio');
        $this->db->where('clave', $subSubService);
        $result= $this->db->get();                
        return $result->result();
        
    }
    
    function getData($qry) {
        $consulta = $this->db->query($qry);
        return $consulta->result();
    }
    
     

//    function insertar_($data) {
//        $this->db->set('id', $data['id']);
//
//        $this->db->insert('tabla');
//    }


}
