<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter_model extends CI_Model {

    function Newsletter_model() {
        //parent::__construct();
        $this->load->database();
    }

    function get_subscriber($mail) {
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->where('email', $mail);
        $result = $this->db->get();

        return $result->result();
    }

    function insert_subscriber ($email) {
        $this->db->set('email', $email);
        $this->db->insert('newsletter');
        return true;
    }

    function getData($qry) {
        $consulta = $this->db->query($qry);
        return $consulta->result();
    }

}
