<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Meta_tags_model extends CI_Model {

    function Meta_tags_model() {
        //parent::__construct();
        $this->load->database();
    }

    function get_meta_tags($clave) {
        $this->db->select('*');
        $this->db->from('meta_tags');
        $this->db->where('seccion', $clave);
        $result = $this->db->get();

        return $result->result();
    }


    function getData($qry) {
        $consulta = $this->db->query($qry);
        return $consulta->result();
    }

}
