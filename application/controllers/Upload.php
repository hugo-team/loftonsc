<?php

class Upload extends CI_Controller {

    function Upload() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
    }

    function index() {
        
    }

    function do_upload() {

        $nombre = $this->input->post('leaflet_name');
        $email = $this->input->post('leaflet_email');
        $tel = $this->input->post('leaflet_phone');
        $mensaje = $this->input->post('leaflet_msg');
        //adavila@lotonsc.com
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mes_actual = $meses[date('n') - 1] . "-" . date('Y');
        $ruta = 'curriculums/' . $mes_actual . '';

        if (!is_dir($ruta)) { //create the folder if it's not already exists
            mkdir($ruta, 0777, TRUE);
        }
        
        $new_name = str_replace(' ','_',$nombre).date("d_m_y");
        
        $config['upload_path'] = $ruta;
        $config['allowed_types'] = 'pdf|docx';
        $config['max_size'] = 3145728;
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('leaflet_cv')) {
            $error = array('error' => $this->upload->display_errors());
            $error = $error['error'];
            $result = FALSE;
            $msg = 'Hola, <strong>' . $nombre . '</strong>.<br />Hubo un error al ingresar tus datos.<br />' . $error . '<br />Por favor intentalo más tarde.';
        } else {

            $data = $this->upload->data();
            $ruta_curriculum = $data['full_path'];

            $this->email->from('contacto@ranchomagico.com.mx', 'LoftonSC | VACANTES');
            $this->email->to('hugo.guc@gmail.com,wguillen@loftonsc.com');
            $this->email->subject('Nuevo Curriculum Recibido');
            $this->email->message('
                Se recibió un nuevo curriculum con los siguientes datos: <br /><br />
                            Nombre:<strong>' . $nombre . '</strong><br />
                            E-mail:<strong>' . $email . '</strong><br />
                            Teléfono:<strong>' . $tel . '</strong><br />
                            Comentarios:<br />' . $mensaje . '
                    ');

            $this->email->attach($ruta_curriculum);

            if ($this->email->send()) {
                $result = TRUE;
                $msg = 'Hola, <strong>' . $nombre . '</strong>.<br />Hemos recibido tu CV en breve nos pondremos en contacto.<br />¡Gracias!';
            } else {
                $result = FALSE;
                $msg = 'Hola, <strong>' . $nombre . '</strong>.<br />Hubo un error al ingresar tus datos.<br />Por favor intentalo más tarde.';
            }
        }

        $response['result'] = $result;
        $response['msg'] = $msg;
        $this->load->view('layout/header');
        $this->load->view('career/cv_message', $response);
        $this->load->view('layout/footer');
    }

}
