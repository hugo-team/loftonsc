<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('email');
        
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'docx|pdf';
        $config['max_size'] = '30000';

        $this->load->library('upload', $config);
        
        // $this->load->model('');
    }

    public function index() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.';
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información';
        $meta_tags['title'] = 'Carrera';

        $this->load->view('layout/header', $meta_tags);
        $this->load->view('career/carrera_view');
        $this->load->view('layout/footer');
    }

    public function plan_de_carrera() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.';
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información';
        $meta_tags['title'] = 'Plan de Carrera';

        $this->load->view('layout/header', $meta_tags);
        ;
        $this->load->view('career/plan_de_carrera_view');
        $this->load->view('layout/footer');
    }

    public function testimoniales() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.';
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información';
        $meta_tags['title'] = 'Testimoniales';

        $this->load->view('layout/header', $meta_tags);
        $this->load->view('career/testimoniales_view');
        $this->load->view('layout/footer');
    }

    public function vacantes() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.';
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información';
        $meta_tags['title'] = 'Vacantes';

        $this->load->view('layout/header', $meta_tags);
        $this->load->view('career/vacantes_view');
        $this->load->view('layout/footer');
    }

    public function curriculum() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.';
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información';
        $meta_tags['title'] = 'Sube tu CV';

        $this->load->view('layout/header', $meta_tags);
        $this->load->view('career/curriculum_view');
        $this->load->view('layout/footer');
    }


}
