<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('servicios_model');
    }
    
    public function index() {
        redirect('about/conocenos');
    }
    
    public function conocenos() {
        $services = $this->servicios_model->getServiceData(NULL);

        $data['services']= $services;
        
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'CONÓCENOS';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('about/conocenos_view',$data);
        $this->load->view('layout/footer');
    }
    
    public function historia() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'Historia';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('about/historia_view');
        $this->load->view('layout/footer');
    }
    
    public function porque_lofton(){
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'POR QUÉ LOFTON';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('about/porque_view');
        $this->load->view('layout/footer');
    }
    
    public function problemas_dificiles(){
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Somos una Firma contable que brindar servicios integrales de manera profesional diseñando estrategias inteligentes y dar soluciones de negocio adecuados.' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'Problemas difíciles';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('about/problemas_dificiles_view');
        $this->load->view('layout/footer');
    }

}
