<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('blog_model');
    }

    public function index() {
        $data['result'] = $this->blog_model->getPosts();
        $home['home_foot'] = true;
        
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'En Lofton satisfacemos necesidades y requerimientos de su Empresa para brindarle un servicio profesional. Somos un despacho de contadores certificado bajo la norma internacional ISO 9001-2008.' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información, Despacho Auditoría, Despacho Contabilidad' ;
        $meta_tags['title'] = 'Contadores, Auditores,Abogados, Fiscalistas ';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('home_view',$data);
        $this->load->view('layout/footer',$home);
    }   
    
    public function privacy() {
        
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'aviso de privacidad Lofton y Asociados' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'AVISO DE PRIVACIDAD ';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('layout/privacy_view');
        $this->load->view('layout/footer_home');
    } 

}
