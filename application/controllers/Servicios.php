<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('servicios_model');
        $this->load->model('meta_tags_model');
    }

    public function index() {
        $services = $this->servicios_model->getServiceData(NULL);

        $arr_services = array();
        foreach ($services as $frm_service) {
            array_push($arr_services, $frm_service->nombre_servicio);
        }
        $form_services['services'] = $arr_services;

        $data['services'] = $services;
        $data['form_services'] = $form_services;
        $this->load->view('layout/header');
        $this->load->view('servicios_home_view', $data);
        $this->load->view('layout/footer');
    }

    public function service($service) {
        $meta_tags_array = $this->meta_tags_model->get_meta_tags($service);
        $detalles_servicio = $this->servicios_model->getServiceData($service);
        
        foreach($meta_tags_array as $meta){
           $author = $meta->author;
           $description = $meta->description;
           $keywords= $meta->keywords;
        }

        
        foreach ($detalles_servicio as $servicio) {
            foreach ($servicio as $s => $value) {
                $data[$s] = $value;
            }
        }
        
        $meta_tags['author'] = $author;
        $meta_tags['description'] = $description ;
        $meta_tags['keywords'] = $keywords ;
        $meta_tags['title'] = $data['nombre_servicio'] ;
        
        $gerentes = json_decode(json_encode($this->servicios_model->getmanager($data['id_servicio'])), True);
        $data['gerentes'] = $gerentes;

        $qry_subservicios = $this->servicios_model->getSubServices($data['id_servicio'], NULL);

        $subservicios = json_decode(json_encode($qry_subservicios), True);

        $arr_services = array();

        foreach ($qry_subservicios as $frm_service) {
            array_push($arr_services, $frm_service->subservicio);
        }
        $form_services['services'] = $arr_services;


        $data['form_services'] = $form_services;

        $data['subservicios'] = $subservicios;
        $data['folder'] = str_replace("_", "-", "$service");
        $data['cols'] = 4;
        $data['num_subs'] = count($subservicios);

        $this->load->view('layout/header',$meta_tags);
        $this->load->view('services/service_view', $data);
        $this->load->view('layout/footer');
    }

    public function sub_service($subService) {
        
        $meta_tags_array = $this->meta_tags_model->get_meta_tags($subService);       
        $detalles_subservicio = $this->servicios_model->getSubServiceData($subService);
        
        foreach($meta_tags_array as $meta){
           $author = $meta->author;
           $description = $meta->description;
           $keywords= $meta->keywords;
        }
        
        foreach ($detalles_subservicio as $servicio) {
            foreach ($servicio as $ss => $value) {
                $subservice_data[$ss] = $value;
            }
        }
        
        
        $service_images = $this->servicios_model->getData('SELECT img_banner, img_fondo from servicio where id_servicio =' . $subservice_data['id_servicio']);
        foreach ($service_images as $img) {
            $banner = $img->img_banner;
            $fondo = $img->img_fondo;
        }
        $gerentes = json_decode(json_encode($this->servicios_model->getmanager($subservice_data['id_servicio'])), True);

        $qry_sub_services = $this->servicios_model->getSubSubServices($subservice_data['id_subservicio'], null);

        $sub_sub_services = json_decode(json_encode($qry_sub_services), True);

        $clave_servicio = $this->servicios_model->getData('select s.clave_servicio, ss.clave_subservicio from servicio s join subservicio ss where s.id_servicio = ' . $subservice_data['id_servicio'] . ' and ss.id_subservicio = ' . $subservice_data['id_subservicio']);

        foreach ($clave_servicio as $f) {
            $service_folder = $f->clave_servicio;
            $subservice_folder = $f->clave_subservicio;
        }

        $arr_services = array();
        foreach ($qry_sub_services as $frm_service) {
            array_push($arr_services, $frm_service->sub_subservicio);
        }
        $form_services['services'] = $arr_services;

        $folder = $service_folder . '/' . $subservice_folder;

        $data['id_servicio'] = $subservice_data['id_servicio'];
        $data['nombre_servicio'] = $subservice_data['subservicio'];
        $data['img_subservicio'] = $subservice_data['img_subservicio'];
        $data['descripcion'] = $subservice_data['descripcion'];
        $data['img_banner'] = $banner;
        $data['img_fondo'] = $fondo;
        $data['gerentes'] = $gerentes;
        $data['subservicios'] = $sub_sub_services;
        $data['folder'] = str_replace("_", "-", "$folder");
        $data['cols'] = 8;
        $data['num_subs'] = sizeof($sub_sub_services);
        $data['form_services'] = $form_services;
        
        $meta_tags['author'] = $author;
        $meta_tags['description'] = $description ;
        $meta_tags['keywords'] = $keywords ;
        $meta_tags['title'] = $subservice_data['subservicio'];
                
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('services/sub_services_view', $data);
        $this->load->view('layout/footer');
    }

    public function sub_sub_service($subSubService) {

        $detalles_sub_subservicio = $this->servicios_model->getSubSubServiceData($subSubService);        
        $meta_tags_array = $this->meta_tags_model->get_meta_tags($subSubService);       
        
        foreach($meta_tags_array as $meta){
           $author = $meta->author;
           $description = $meta->description;
           $keywords= $meta->keywords;
        }
        foreach ($detalles_sub_subservicio as $servicio) {
            foreach ($servicio as $ss => $value) {
                $subservice_data[$ss] = $value;
            }
        }

        $sub_service = $this->servicios_model->getData('SELECT id_servicio FROM subservicio WHERE id_subservicio = ' . $subservice_data['id_subservicio']);
        $service_images = $this->servicios_model->getData('SELECT img_banner, img_fondo from servicio where id_servicio =' . $sub_service[0]->id_servicio);
        foreach ($service_images as $img) {
            $banner = $img->img_banner;
            $fondo = $img->img_fondo;
        }
        $gerentes = json_decode(json_encode($this->servicios_model->getmanager($sub_service[0]->id_servicio)), True);
        $qry_subservicios = $this->servicios_model->getSubSubServices($subservice_data['id_subservicio'], $subSubService);
        $subservicios = json_decode(json_encode($qry_subservicios), True);

        $clave_servicio = $this->servicios_model->getData('select s.clave_servicio, ss.clave_subservicio from servicio s join subservicio ss where s.id_servicio = ' . $sub_service[0]->id_servicio . ' and ss.id_subservicio = ' . $subservice_data['id_subservicio']);

        foreach ($clave_servicio as $f) {
            $service_folder = $f->clave_servicio;
            $subservice_folder = $f->clave_subservicio;
        }

        $folder = $service_folder . '/' . $subservice_folder;

        $arr_services = array();
        foreach ($qry_subservicios as $frm_service) {
            array_push($arr_services, $frm_service->sub_subservicio);
        }
        $form_services['services'] = $arr_services;



        $data['id_servicio'] = $sub_service[0]->id_servicio;
        $data['nombre_servicio'] = $subservice_data['sub_subservicio'];
        $data['img_subservicio'] = $subservice_data['img_subservicio'];
        $data['descripcion'] = $subservice_data['descripcion'];
        $data['img_banner'] = $banner;
        $data['img_fondo'] = $subservice_data['img_subservicio'];
        $data['gerentes'] = $gerentes;
        $data['subservicios'] = $subservicios;
        $data['folder'] = str_replace("_", "-", "$folder");
        $data['cols'] = 8;
        $data['num_subs'] = count($subservicios);
        $data['form_services'] = $form_services;
        
        $meta_tags['author'] = $author;
        $meta_tags['description'] = $description ;
        $meta_tags['keywords'] = $keywords ;
        $meta_tags['title'] = $subservice_data['sub_subservicio'];
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('services/sub_sub_services_view', $data);
        $this->load->view('layout/footer');
    }

}
