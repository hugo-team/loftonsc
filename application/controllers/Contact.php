<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
    }

    public function index() {
        $meta_tags['author'] = 'Lofton y Asociados';
        $meta_tags['description'] = 'Lofton, Firma de Consultoría con 18 años de experiencia, está ubicada en: Guillermo Prieto #76, Col. San Rafael, C.P. 06470, México D.F. +01 (55) 1500-0666' ;
        $meta_tags['keywords'] = 'Auditoría, Consultoría de Negocios, Consultoría en Mercadotecnia, Contabilidad, Fianzas y Seguros, Jurídico, Jurídico Civil, Jurídico Corporativo, Jurídico Familiar, Jurídico Fiscal, Jurídico Laboral, Jurídico Mercantil Nóminas, Recursos Humanos, Tecnologías de la Información' ;
        $meta_tags['title'] = 'Contacto';
        
        $this->load->view('layout/header',$meta_tags);
        $this->load->view('contact_view');
        $this->load->view('layout/footer');
    }

    public function contact_service() {
        
        $this->email->from( $this->input->post('email'), 'Contacto LoftonSc');
        $this->email->to('contacto@loftonsc.com, hcampos@loftonc.com, lguerrero@loftonsc.com');
        $this->email->subject('Solicitud de servicio');
        $this->email->message('
                            <p>A quien corresponda:</p>
                            <p>&nbsp;</p>
                            <p>Mi nombre es: <strong>'.$this->input->post('nombre_completo').'</strong></p>
                            <p>&nbsp;</p>
                            <p>Me pongo en contacto con ustedes porque solicito el servicio de: 
                               <strong>'.$this->input->post('servicio_solicitado').'</strong></p>                            
                            <p>Mi correo es: <strong>'.$this->input->post('mail').'</strong></p>                            
                            <p>Mi tel&eacute;fono es: <strong>'.$this->input->post('telefono').'</strong></p>                          
                            <p> Sin m&aacute;s por el momento, agradezco su atenci&oacute;n, 
                                esperando una pronta respuesta.</p>
                        ');
        if ($this->email->send()) {
            $data = array("sent" => TRUE, "msg"=>'Hola, <strong>'.$this->input->post('nombre_completo').'</strong>.<br />
                    Tus datos han sido ingresados correctamente y serán remitidos al área correspondiente. <br />
                    En un breve plazo nos comunicaremos contigo para dar seguimiento a tu solicitud. <br />
                    Muchas gracias.');
        } else {
            $data = array("sent" => FALSE, "msg"=>'Hola, <strong>'.$this->input->post('nombre_completo').'</strong>.
                    <br />Hubo un error al ingresar tus datos.<br />Por favor intentalo más tarde.');
        }        
        echo json_encode($data);       
    }
    
    public function contact_lofton() {
        
        $this->email->from( $this->input->post('e-mail'), 'Contacto LoftonSc');
        $this->email->to('contacto@loftonsc.com, hcampos@loftonc.com, lguerrero@loftonsc.com');
        $this->email->subject('Contacto Lofton');
        $this->email->message('
                            Nombre:<strong>'.$this->input->post('name').'</strong><br />
                            E-mail:<strong>'.$this->input->post('e-mail').'</strong><br />
                            Comentarios:<br />'.$this->input->post('message').'
                        ');
        if ($this->email->send()) {
            $data = array("sent" => TRUE, "msg"=>'Hola, <strong>'.$this->input->post('name').'</strong>.<br />
                    Hemos recibido tu email. <br />
                    ¡Gracias por tus comentarios!');
        } else {
            $data = array("sent" => FALSE, "msg"=>'Hola, <strong>'.$this->input->post('name').'</strong>.
                    <br />Hubo un error al ingresar tus datos.<br />Por favor intentalo más tarde.');
        }        
        echo json_encode($data);       
    }
    
    public function suggestions() {
        
        $this->email->from( $this->input->post('email'), 'Contacto LoftonSc');
        $this->email->to('hugo.guc@gmail.com');
        $this->email->subject('Quejas y Sugerencias');
        $this->email->message('
                            <h2>'.$this->input->post('qs_slect').'</h2>
                            Nombre:<strong>'.$this->input->post('qs_nombre').'</strong><br />
                            E-mail:<strong>'.$this->input->post('qs_email').'</strong><br />
                            Teléfono:<strong>'.$this->input->post('qs_tel').'</strong><br />
                            Comentarios:<br />'.$this->input->post('qs_msj').'
                        ');
        if ($this->email->send()) {
            $data = array("sent" => TRUE, "msg"=>'Hola, <strong>'.$this->input->post('qs_nombre').'</strong>.<br />
                    Tus datos han sido ingresados correctamente y serán remitidos al área correspondiente. <br />
                    Muchas gracias por tus comentarios.');
        } else {
            $data = array("sent" => FALSE, "msg"=>'Hola, <strong>'.$this->input->post('qs_nombre').'</strong>.
                    <br />Hubo un error al procesar tu solicitud.<br />Por favor intentalo más tarde.');
        }        
        echo json_encode($data);       
    }

}
