<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administrador extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        redirect('administrador/services_management');
    }

    public function _lofton_output($output = null) {
        $this->load->view('administrador/servicios_admin', (array) $output);
    }

    public function services_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('servicio');
        $crud->columns('nombre_servicio','descripcion', 'clave_servicio', 'img_banner', 'img_fondo');
        $crud->display_as('nombre_servicio', 'Servicio')
                ->display_as('clave_servicio', 'Clave')
                ->display_as('img_banner', 'Banner')
                ->display_as('img_fondo', 'Fondo');
        $crud->set_subject('Servicio');
        $crud->set_field_upload('img_banner','assets/img/servicios/upload/banners');
        $crud->set_field_upload('img_fondo','assets/img/servicios/upload/backgrounds');

        $output = $crud->render();

        $this->_lofton_output($output);
    }

    public function subservices_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('subservicio');
        $crud->columns('id_servicio', 'clave_subservicio', 'subservicio', 'descripcion_breve', 'descripcion', 'img_subservicio', 'link_detalle');
        $crud->display_as('id_servicio', 'Servicio')
                ->display_as('clave_subservicio', 'Clave')
                ->display_as('subservicio', 'Subservicio')
                ->display_as('descripcion_breve', 'Breve descipción')
                ->display_as('descripcion', 'Descripción')
                ->display_as('img_subservicio', 'Imagen')
                ->display_as('link_detalle', 'Link detalle');
        $crud->set_subject('Sub servicio');
        $crud->set_field_upload('img_subservicio','assets/img/servicios/upload/buttons');
        $crud->set_relation('id_servicio','servicio','nombre_servicio');

        $output = $crud->render();

        $this->_lofton_output($output);
    }
    
    public function subsubservices_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('sub_subservicio');
        $crud->columns('id_subservicio', 'clave', 'sub_subservicio', 'descripcion_breve', 'descripcion', 'img_subservicio', 'link_detalle');
        $crud->display_as('id_subservicio', 'Subservicio')
                ->display_as('clave', 'Clave')
                ->display_as('subservicio', 'Subservicio')
                ->display_as('descripcion_breve', 'Breve descipción')
                ->display_as('descripcion', 'Descripción')
                ->display_as('img_subservicio', 'Imagen')
                ->display_as('link_detalle', 'Link detalle');
        $crud->set_subject('Sub subservicio');
        $crud->set_field_upload('img_subservicio','assets/img/servicios/upload/buttons/sub');
        $crud->set_relation('id_subservicio','subservicio','subservicio');

        $output = $crud->render();

        $this->_lofton_output($output);
    }
    
     public function managers_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('gerentes');
        $crud->columns('area', 'nombre', 'puesto');
        $crud->display_as('area', 'Area')
                ->display_as('nombre', 'Nombre')
                ->display_as('puesto', 'Puesto');
        $crud->set_subject('Gerente');
        $crud->set_relation('area','servicio','nombre_servicio');

        $output = $crud->render();

        $this->_lofton_output($output);
    }
    
    public function vacancy_management() {
        $crud = new grocery_CRUD();

        $crud->set_table('vacantes');
        $crud->columns('titulo', 'edad', 'escolaridad', 'zona_trabajo', 'requisitos', 'contacto');
        $crud->display_as('titulo', 'Título')
                ->display_as('edad', 'Edad')
                ->display_as('escolaridad', 'Escolaridad')
                ->display_as('zona_trabajo', 'Zona de trabajo')
                ->display_as('requisitos', 'Requisitos')
                ->display_as('contacto', 'Contacto');
        $crud->set_subject('Vacante');
       
        $output = $crud->render();

        $this->_lofton_output($output);
    }
    
    public function meta_magnament() {
        $crud = new grocery_CRUD();

        $crud->set_table('meta_tags');
        $crud->columns( 'seccion', 'author', 'description', 'keywords');
        $crud->display_as('seccion', 'Sección')
                ->display_as('author', 'Author')
                ->display_as('description', 'Description')
                ->display_as('keywords', 'Keywords');
        $crud->set_subject('Meta tags');

        $output = $crud->render();

        $this->_lofton_output($output);
    }
    

}
