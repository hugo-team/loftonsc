<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('newsletter_model');
    }

    public function index() {
        $mail = $this->input->post('suscriber_mail');
        $suscriber = $this->newsletter_model->get_subscriber($mail);
        if (sizeof($suscriber) > 0) {
            $data = array("subscribed" => false, "msg" => 'El correo <strong>' . $mail . '</strong>  ya ha sido registrado anteriormente.');
        } else {
            $new_suscriber = $this->newsletter_model->insert_subscriber($mail);
            if ($new_suscriber) {
                $data = array("subscribed" => true, "msg" => 'Se ha suscrito con el correo <strong>' . $mail . '</strong>.');
            } else {
                $data = array("subscribed" => false, "msg" => 'Hubo un error al registrar tus correo.<br />Por favor intentalo más tarde.');
            }
        }
        echo json_encode($data);
    }

    public function newsletter() {
        $data = $this->newsletter_model->getData('SELECT * FROM newsletter');
    }

}
