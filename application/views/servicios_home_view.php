<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/services.css'); ?>" media="screen">

    <div style="margin-top: 18px;">
        <div class="banner_service"  >
            <div class="service_tittle" style="min-width:35%;" >
                <h1 class="titulo_seccion" >Servicios</h1>
            </div>  
            <img src="<?php echo base_url('assets/img/servicios/nuestros_servicios.jpg'); ?>" class="img-responsive"  alt="">   
        </div>       
    </div>
    <div class="bg_service " style="background: url('<?php echo base_url('assets/img/servicios/bg_servicios.jpg'); ?>');">
        <div class="bg_service_blur" >	
            <div class="description">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">

                        <p class="text-left">Basados en un amplio conocimiento de cada sector de industria y la combinación entre colaboración y nuestras fortalezas en contabilidad, auditoría, administración de nóminas, asesoría legal, consultoría y seguros y fianzas nos permiten crear soluciones realmente integrales. Ofrecemos una perspectiva de 360 grados que nos permite atacar los retos de una forma amplia y profunda.</p>
                        <p class="text-left">Ya sea que usted busque los servicios de auditoría o los servicios integrales de consultoría, nuestros profesionales le atenderán brindándole un servicio excepcional a través de nuestras líneas de servicio.</p>                
                        <br>             
                        <div class="col-xs-12 col-md-12">	            		                       
                            <h4>Siempre dispuestos a atenderte</h4>
                            <h3>(55) 1500-0666</h3>
                            <p> 
                                O si lo prefiere, puede contactarnos usando el formulario de contacto y en breve nos counicaremos con usted.
                            </p>
                        </div>                                      

                </div>
                <?php $this->load->view('contacto_view.php',$form_services); ?>
            </div>
        </div>
    </div>
    <h1 class="text-center titulo_seccion">NUESTROS SERVICIOS</h1>
    <?php foreach ($services as $service): ?>
        <div class="col-md-4 col-xs-12 service">            
            <div class="row service_image">
                <div class="caption">
                    <div class="caption_text">
                        <h3><?php echo $service->nombre_servicio; ?></h3>                        
                        <p><a title="Conocer más sobre <?php echo $service->nombre_servicio; ?>" class="label label-default" href="<?php echo base_url('servicios/service/' . $service->clave_servicio); ?>">Conocer más</a></p>
                    </div>
                </div>
                <img class="img-responsive" src="<?php echo base_url('assets/img/servicios/upload/banners/' . $service->img_banner); ?>">
                <div class="yellow_tittle">
                    <h4><?php echo $service->nombre_servicio; ?></h4>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    
    <div class="container">
        <br /><br />
    </div>  

