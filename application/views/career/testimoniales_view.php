<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">TESTIMONIALES</h1>
            <p class="desc_oc">Conoce los testimonios de nuestros más exitosos colaboradores, quienes con dedicación y profesionalismo construyen su carrera. TESTIMONIALES</p>
        </div> 
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <!-- Start Memebr 1 -->
            <div class="col-md-6">
                <div class="team-member">
                    <!-- Memebr Photo, Name & Position -->
                    <div class="member-photo">
                        <img class="img-responsive" alt="" src="<?php echo base_url('assets/img/testimoniales/armandodav.jpg'); ?>">
                        <div class="member-name">Lic. Armando Dávila <span>Recursos Humanos</span></div>
                    </div>
                    <!-- Memebr Words -->
                    <div class="member-info">
                        <p>
                            “En Lofton he podido potencializar mis habilidades, teniendo la oportunidad de desarrollarme profesionalmente, adquiriendo nuevos retos cada día.
                        </p>
                        <p>
                            Gracias a la confianza que la Firma ha depositado en mí, hoy tengo una gran oportunidad que me permitirá compartir mi experiencia para el desarrollo y crecimiento de todos los que hoy en día formamos parte de esta gran familia.
                            Sé que el trabajo duro, la organización y la perseverancia son elementos que garantizarán el logro de objetivos."
                        </p>
                    </div>

                </div>
            </div>
            <!-- End Memebr 1 -->

            <!-- Start Memebr 2 -->
            <div class="col-md-6">
                <div class="team-member">
                    <!-- Memebr Photo, Name & Position -->
                    <div class="member-photo">
                        <img class="img-responsive" alt="" src="<?php echo base_url('assets/img/testimoniales/salvador.jpg'); ?>">
                        <div class="member-name">Lic. Salvador Ramírez Martínez <span>Tesorería</span></div>
                    </div>
                    <!-- Memebr Words -->
                    <div class="member-info">
                        <p>
                            “Mi primer día en Lofton fue muy mortificante, porque sabía que estaba en un monstruo de empresa. Pasó el tiempo, me hice de muy buenos compañeros de trabajo, amigos y algunos de ellos se convirtieron en personas muy importantes en mi vida. Esto creció de la misma manera en que progresó mi oportunidad laboral.
                        </p>
                        <p>
                            Hoy a 5 años de mi ingreso a esta Firma tan importante, demuestro mi mayor esfuerzo, me desempeño en un puesto significativo gracias a la confianza y cariño depositado por parte de las cabezas de la Firma. Solo puedo decir gracias por tan importante apoyo profesional y oportunidad de crecimiento."
                        </p>
                    </div>

                </div>
            </div>
            <!-- End Memebr 2 -->

            <!-- Start Memebr 3 -->
            <div class="col-md-6">
                <div class="team-member">
                    <!-- Memebr Photo, Name & Position -->
                    <div class="member-photo">
                        <img class="img-responsive" alt="" src="<?php echo base_url('assets/img/testimoniales/anayeli.jpg'); ?>">                                        
                        <div class="member-name">Lic. Anayeli Cervantes Palacios <span>Auditoría</span></div>
                    </div>
                    <!-- Memebr Words -->
                    <div class="member-info">
                        <p>
                            “El éxito se consigue buscándolo” y en Lofton lo he encontrado. Como integrante de esta Firma me complace compartir la experiencia adquirida.
                        </p>
                        <p>
                            Durante mi estancia en el corporativo he sido testigo de su posicionamiento profesional, convirtiéndose en el ámbito idóneo para el desarrollo de mis habilidades, permitiéndome aprovechar los retos y oportunidades que día a día se hacen presentes.
                            Con el tiempo he podido constatar que Lofton representa el lugar ideal para todo aquel profesionista que busca su crecimiento a través del compromiso y actitud de servicio."
                        </p>
                    </div>

                </div>
            </div>
            <!-- End Memebr 3 -->

            <!-- Start Memebr 4 -->
            <div class="col-md-6">
                <div class="team-member">
                    <!-- Memebr Photo, Name & Position -->
                    <div class="member-photo">

                        <img class="img-responsive" alt="" src="<?php echo base_url('assets/img/testimoniales/hugo.jpg'); ?>">
                        <div class="member-name">C.P. Hugo Andrés Mendieta Jímenez <span>Contabilidad</span></div>
                    </div>
                    <!-- Memebr Words -->
                    <div class="member-info">
                        <p>
                            “Trabajar en Lofton ha sido una de las mejores experiencias. Desde mi primer día, hace más de 5 años, he logrado adquirir experiencia y desarrollo constante a nivel profesional y personal.<br />
                        </p>
                        <p>
                            En Lofton he encontrado, más que mi lugar de trabajo, un segundo hogar y la mejor plataforma de crecimiento, desarrollo y aprendizaje, en un inmejorable ambiente de trabajo.”
                        </p>
                    </div>

                </div>
            </div>
            <!-- End Memebr 4 -->

        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>
