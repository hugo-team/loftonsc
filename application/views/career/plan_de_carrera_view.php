<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">PLAN DE CARRERA</h1>
            <p class="desc_oc">Conoce el plan de crecimiento que tenemos preparado para ti en nuestra firma. PLAN DE CARRERA</p>
        </div>

    </div>
    <div class="row">
        <!--I-->
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <img class="img-responsive" alt="Conoce el plan de crecimiento que tenemos preparado para ti en nuestra firma." src="<?php echo base_url('assets/img/plan_de_carrera.png'); ?>">
        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>

</div>