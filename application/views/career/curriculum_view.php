<link href="<?php echo base_url('assets/css/file_input/fileinput.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/file_input/fileinput-rtl.min.css'); ?>" rel="stylesheet">
<style>
    .btn-primary {
        background-color: #1a5b3d;
        border-color: #1a5b3d;
    }
    .btn-primary:hover{
        background-color: #595d61;
        border-color: #3595d61;
    }
    .btn.btn-default.fileinput-upload.fileinput-upload-button, button.kv-file-zoom.btn.btn-xs.btn-default{display:none;}

</style>
<div class="container" style="margin-top: 30px;">

    <div class="row">

        <div class="col-xs-12">
            <h1 class="green_text titulo_seccion">¡Crece con nosotros! </h1>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-12 col-md-9 col-lg-9">
            <h2 class="servicios_ofrecemos_txt">Intégrate a nuestro equipo.</h2>
            <form enctype="multipart/form-data" id="upload_cv" action="<?php echo base_url('upload/do_upload'); ?>" method="post">
                <div class="form-group">
                    <input type="text" class="form-control"  id="leaflet_name" name="leaflet_name" placeholder="* Nombre completo" >
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="leaflet_mail" name="leaflet_email" placeholder="* Correo Electrónico" > 
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control"  id="leaflet_phone" name="leaflet_phone" placeholder="*Teléfono" >
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="leaflet_msg" name="leaflet_msg" rows="3" placeholder="* Comentarios" ></textarea> 
                </div>
                <div class="form-group">
                    <div dir=rtl>
                        <label class="control-label">Sube Tu CV</label>
                    </div>

                    <input id="leaflet_cv" name="leaflet_cv"  type="file" class="file-loading" accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword" >
                </div>
                <input type="submit" name="send_cv" class="button" id="send_cv" value="Enviar" style="float: right;">

            </form>
        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>
<script src="<?php echo base_url('assets/js/fileinput.min.js'); ?>"></script>
<script>
    $(document).on('ready', function () {
        $("#leaflet_cv").fileinput({
            rtl: true,
            allowedFileExtensions: ["pdf", "docx"],
            browseLabel: 'Buscar CV',
            uploadLabel: 'Subir CV',
            removeLabel: 'Eliminar',
            msgInvalidFileExtension: 'Extensión no válida para "{name}". Sólo se admiten extensiones "{extensions}".',

        });
    });
</script>