<div class="container" style="margin-top: 30px;">
    <div class="row">

        <div class="col-xs-12 col-md-9 col-lg-9">
            <h1 class="green_text titulo_seccion"> <?php echo $result == TRUE ? 'Mensaje recibido ' : 'Algo salió mal'; ?> </h1>
            <br />
            <?php if ($result == TRUE): ?>
                <div class="alert alert-success">
                    <?php echo $msg; ?>
                </div>
            <?php elseif ($result == FALSE) : ?>
                <div class="alert alert-danger">
                    <?php echo $msg; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>
