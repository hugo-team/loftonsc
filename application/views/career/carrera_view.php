<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">CARRERA</h1>
            <p class="desc_oc">En Lofton &amp; Asociados ponemos a tu alcance todo un mundo de posibilidades para desarrollar tu carrera profesional, aquí encontrarás todo lo que necesitas. CARRERA</p>
        </div>
    </div>

    <div class="row">
        <!--I-->
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">

            <img class="img-responsive" alt="En Lofton &amp; Asociados ponemos a tu alcance todo un mundo de posibilidades para desarrollar tu carrera profesional, aquí encontrarás todo lo que necesitas." src="<?php echo base_url('assets/img/carrera/carrera.jpg'); ?>">
            <br>
            <p class="text-left">En Lofton &amp; Asociados ponemos a tu alcance todo un mundo de posibilidades para desarrollar tu carrera profesional, aquí encontrarás todo lo que necesitas.</p>

            <h2 class="titulo_seccion green_text">Proceso de reclutamiento</h2>
            <p class="text-left">Contamos con un proceso de reclutamiento que nos permite contar con profesionales que se identifiquen y manifiesten las competencias y valores de la Firma y a su vez proporcionen a nuestros clientes la atención y servicios de calidad que nos caracterizan.</p>
            <p class="text-left">Para participar, sólo necesitas aplicar en línea ingresa tu cv. Cuando contemos con alguna posición de acuerdo a tus competencias y expectativas profesionales te contactaremos de inmediato. Tus datos permanecerán por 6 meses vigentes en nuestra cartera.</p>
        </div>

       <?php $this->load->view('career/career_links_view'); ?>
    </div>

</div>

