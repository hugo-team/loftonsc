<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion text-center">¡Vacantes Recientes! </h1>
            <h2 class="text-center">Intégrate a nuestro equipo.</h2>
        </div>
    </div>

    <div class="row">
        <h4 class="classic-title"><span>Puestos Solicitados</span></h4>
        <!--I-->
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>           
					<br>           
					<h4 class="green_text">Ayudante de Cocina/Medio tiempo</h4>
					<p class="vac">Edad: 18 en adelante  </p>
					<p class="vac">Sexo: Indistinto </p>
					<p class="vac">Escolaridad: Secundaria Concluida</p>
					<p class="vac">Zona de Trabajo:     Col. San Rafael</p>
					<p class="vac">Requisitos Específicos:  Ayudante General en Cocina</p>
					<p class="vac">Actitud de servicio</p>
					<p class="vac">Email de contacto: aguerrero@loftonsc.com</p>
					<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>
			  
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <br>
                    <br>
                    <h4 class="green_text">Consultor Analista  </h4>
                    <p class="vac"> Edad    :   25-33 años  </p>
                    <p class="vac"> Sexo    :   Indistinto  </p>
                    <p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
                    <p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
                    <p class="vac"> Requisitos Específicos  :   Reingenieria de procesos, implementar ERP´S y Reingenieria organizacional.     </p>
                    <p class="vac"> Email de contacto   :   recursoshumanos@loftonsc.com    </p>
                    <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
				
				<div style="clear:both;"></div>
				
				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">         Diseñador Industrial    </h4>
					<p class="vac"> Edad    :   27 años en adelante </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Licenciatura en Diseño, Diseño Industrial o Artes Graficas (Titulado)   </p>
					<p class="vac"> Zona de Trabajo     :   Col Los Reyes, Estado de Mexico </p>
					<p class="vac"> Requisitos Específicos  :   Propuestas de Diseño, servicio al cliente, Manejo de Graficos, Paqueteria Office, Autocad, Modelado en Software 3D, Rhino   </p>

					<p class="vac"> Email de contacto   :   recursoshumanos@loftonsc.com    </p>
					    <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>
                
				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">     Consultor de Procesos   </h4>
					<p class="vac"> Edad:    25-33 años  </p>
					<p class="vac"> Sexo:   Indistinto  </p>
					<p class="vac"> Escolaridad: Lic. Administracion, Ing, Industrial (Titulado) </p>
					<p class="vac"> Zona de Trabajo:     Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos:  auditorias, Controles internos, manejo de Visio y Projec   </p>
				
					<p class="vac"> Email de contacto:   recursoshumanos@loftonsc.com    </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
				
				<div style="clear:both;"></div>
				
                <div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">     Auditor de Control Interno  </h4>
					<p class="vac"> Edad:  25-33 años  </p>
					<p class="vac"> Sexo:   Indistinto  </p>
					<p class="vac"> Escolaridad: Lic. Contabilidad (INDISPENSABLE)   </p>
					<p class="vac"> Zona de Trabajo:     Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos:  Controles internos, buenas practicas, mejoras, norma ISO9001. (INDISPENSABLE) </p>               
					<p class="vac"> Email de contacto:   recursoshumanos@loftonsc.com    </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-6 bot">                    
                    <br>
                    <br>
                    <h4 class="green_text">         Encargado de Auditoria  </h4>
                    <p class="vac"> Edad    :   25 a 33 años    </p>
                    <p class="vac"> Sexo    :   Indistinto  </p>
                    <p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
                    <p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
                    <p class="vac"> Requisitos Específicos  :   Auditoria, impuestos, manejo de grupo, 2 a 3 años de experiencia    </p>
                    <p class="vac"> Email de contacto   :   recursoshumanos@loftonsc.com    </p>
                        <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
                
				<div style="clear:both;"></div>
							
                <div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">Auditor Externo </h4>
					<p class="vac"> Edad    :   25 a 33 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
					<p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos  :   Auditoria, impuestos,   1 a 3 años de experiencia     </p>

					<p class="vac"> Email de contacto   :   aguerrero@loftonsc.com  </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
                    <h4 class="green_text">         Auxiliar Contable   </h4>
                    <p class="vac"> Edad    :   25 a 30 años    </p>
                    <p class="vac"> Sexo    :   Indistinto  </p>
                    <p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
                    <p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
                    <p class="vac"> Requisitos Específicos  :   Contabilidad General, 1 a 3 años de experiencia     </p>
                    <p class="vac"> Email de contacto   :   hzequera@loftonsc.com   </p>
                        <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
				
				<div style="clear:both;"></div>
				
                <div class="col-xs-12 col-md-6 col-lg-6 bot">
                    <br>
                    <br>
                    <h4 class="green_text">         Staff de Auditoria  </h4>
                    <p class="vac"> Edad    :   25 a 33 años    </p>
                    <p class="vac"> Sexo    :   Indistinto  </p>
                    <p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
                    <p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
                    <p class="vac"> Requisitos Específicos  :   Auditoria, impuestos , 1 a 2 años de experiencia      </p>                    
                    <p class="vac"> Email de contacto   :   aguerrero@loftonsc.com  </p>
                        <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">         Staff de Contabilidad   </h4>
					<p class="vac"> Edad    :   25 a 30 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
					<p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos  :   Contabilidad General, 1 a 2 años de experiencia     </p>
					<p class="vac"> Email de contacto   :   hzequera@loftonsc.com   </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
				
				<div style="clear:both;"></div>
				
                <div class="col-xs-12 col-md-6 col-lg-6 bot">
                    <br>
                    <br>
					<h4 class="green_text">         Promo - Vendedor    </h4>
					<p class="vac"> Edad    :   25 a 45 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Bachillerato (Preferentemente)  </p>
					<p class="vac"> Zona de Trabajo     :   Rotativo (Zona Oriente-Poniente-Sur y Norte)    </p>
					<p class="vac"> Requisitos Específicos  :   Labor de venta, promocion y demostracion, 1 año en ventas (preferentemente)      </p>

					<p class="vac"> Email de contacto   :   aguerrero@loftonsc.com  </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">         Diseñador Grafico   </h4>
					<p class="vac"> Edad    :   23 a 29 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Licenciatura en Diseño Grafico  </p>
					<p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos  :   Diseño Grafico, Fotografia, Diseño Web, Photoshop, Ilustrator y After Effects    </p>
					<p class="vac"> Email de contacto   :   oortiz@loftonsc.com </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>
				
				<div style="clear:both;"></div>
				
				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">         Asistente de Telemarketing  </h4>
					<p class="vac"> Edad    :   23 a 30 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Bachillerato (cocluido), Lic en Admon (Trunca)  </p>
					<p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos  :   Ventas via telefonica, atencion a clientes y call center,  Word, Excel y Power Point</p>

					<p class="vac"> Email de contacto   :   aguerrero@loftonsc.com  </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>

				<div class="col-xs-12 col-md-6 col-lg-6 bot">
					<br>
					<br>
					<h4 class="green_text">         Abogado Jr. (Juridico Mercantil)    </h4>
					<p class="vac"> Edad    :   27 a 35 años    </p>
					<p class="vac"> Sexo    :   Indistinto  </p>
					<p class="vac"> Escolaridad :   Licenciatura en Derecho (Titulado)  </p>
					<p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
					<p class="vac"> Requisitos Específicos  :   Audencia Orales, Demandas, Contratos, Tramites en Registro Publico, IFREM, etc   </p>

					<p class="vac"> Email de contacto   :   oortiz@loftonsc.com </p>
						<a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
				</div>	
           
        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>

