<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion text-center">¡Vacantes Recientes! </h1>
            <h2 class="text-center">Intégrate a nuestro equipo.</h2>
        </div>
    </div>

    <div class="row">
        <h4 class="classic-title"><span>Puestos Solicitados</span></h4>
        <!--I-->
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <?php for ($i = 1; $i < 8; $i++): ?>
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <br>
                    <br>
                    <h4 class="green_text">Consultor Analista  </h4>
                    <p class="vac"> Edad    :   25-33 años  </p>
                    <p class="vac"> Sexo    :   Indistinto  </p>
                    <p class="vac"> Escolaridad :   Lic. Contabilidad (INDISPENSABLE)   </p>
                    <p class="vac"> Zona de Trabajo     :   Col. San Rafael </p>
                    <p class="vac"> Requisitos Específicos  :   Reingenieria de procesos, implementar ERP´S y Reingenieria organizacional.     </p>
                    <p class="vac"> Email de contacto   :   recursoshumanos@loftonsc.com    </p>
                    <a title="postularme" class="main-button" href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">¡Postúlate ahora!</a>
                </div>
            <?php endfor; ?>
        </div>
        <?php $this->load->view('career/career_links_view'); ?>
    </div>
</div>

