
<div class="section" style="padding-top:50px; padding-bottom:25px; border-top:0; border-bottom:0; background:#fff;">

    <?php $this->load->view('layout/indicators_view'); ?>
</div>
<!-- Start Footer -->
<footer>
    <div class="container">
        <div class="row footer-widgets">
            <!-- Start Contact Widget -->
            <div class="col-md-6">
                <div class="footer-widget contact-widget">
                    <h4>Información de contacto<span class="head-line"></span></h4>
                    <ul>
                        <li><span>Oficinas Corporativas</span> Guillermo Prieto #76, Col. San Rafael, C.P. 06470, México D.F.</li>
                        <li><span>Teléfono:</span> +01 (55) 1500-0666 </li>                                  
                        <li><span>Correo:</span><a href="mailto:contacto@loftonsc.com"> contacto@loftonsc.com</a> </li>
                        <li><span>Website:</span> http://www.loftonsc.com</li>
                    </ul>
                </div>
            </div>
            <!-- End Contact Widget -->

            <!-- Start Subscribe & Social Links Widget -->
            <div class="col-md-6">
                <?php if (!isset($home_foot)): ?>
                    <div class="footer-widget mail-subscribe-widget">
                        <h4>Boletín<span class="head-line"></span></h4>
                        <p>Mantente informado con las noticias más actuales. ¡Suscríbete a nuestro boletín!</p>
                        <!--form class="form-inline subscribe" role="form" name="subscribe" id="subscribe">
                            <div class="form-group">
                                <input class="form-control" type="mail" placeholder="Escribe tu correo" name="suscriber_mail" id="suscriber_mail">
                            </div>
                            <br />
                            <div class="form-group">
                                <input type="submit" class="main-button" id="news_suscribe" name="news_suscribe" value="Suscribirse">
                            </div>
                        </form-->

                        <form class="form-inline" name="subscribe" id="subscribe">
                            <div class="form-group col-md-8 row">                       
                                <input type="email" class="form-control" placeholder="Escribe tu correo" name="suscriber_mail" id="suscriber_mail" style="width: 90%;" >
                            </div>
                            <input type="submit" class="main-button" id="news_suscribe" name="news_suscribe" value="Suscribirse">
                        </form>


                    </div>
                <?php endif; ?>
                <div class="footer-widget social-widget">
                    <h4>Síguenos<span class="head-line"></span></h4>
                    <ul class="social-icons">
                        <li>
                            <a class="facebook" target="_blank" href="https://www.facebook.com/LoftonSC"><i class="icon-facebook-2"></i></a>
                        </li>
                        <li>
                            <a class="twitter" target="_blank" href="https://twitter.com/Lofton_sc"><i class="icon-twitter-2"></i></a>
                        </li>
                        <li>
                            <a class="linkdin" target="_blank" href="https://www.linkedin.com/company/lofton-y-asociados-s-c-?trk=ppro_cprof"><i class="icon-linkedin-1"></i></a>
                        </li>
                        <li>
                            <a class="google" target="_blank" href="https://www.youtube.com/user/loftoncontadores/featured"><i class="icon-youtube"></i></a>
                        </li>
                        <li>
                            <a class="google" target="_blank" href="https://plus.google.com/+Loftonsc/posts"><i class="icon-gplus-1"></i></a>
                        </li>
                    </ul>
                </div>                
            </div>            
        </div>
        <!-- End Subscribe & Social Links Widget -->
    </div>
    <!-- End Subscribe & Social Links Widget -->

    <!-- Start Copyright -->
    <div class="copyright-section">
        <div class="container">
            <div class="col-md-6">
                <p>© 2017 Lofton y Asociados</p>
            </div>
            <div class="col-md-6">
                <ul class="footer-nav">
                    <li><a href="<?php echo base_url('aviso-de-privacidad.html'); ?>">Aviso de Privacidad</a></li>
                </ul>
            </div>						
        </div>
    </div>
    <!-- End Copyright -->

</footer>
<!-- End Footer -->


</div>
<!-- End Container -->
</div> 
<!-- end blur div -->
<!-- Go To Top Link -->
<a href="#" class="back-to-top"><i class="icon-up-open-1"></i></a>

<div id="loader">
    <center>
      <img src="<?php echo base_url('assets/img/logo_blanco.png');?>" alt="LOFTON" >  
    </center>    
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div>



<div id="quejasModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="z-index: 1500;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Quejas y Sugerencias</h4>
            </div>
            <div class="modal-body">

                <form name="qs" id="qs" >
                    <div class="form-group">
                        <label>*Nombre:</label>
                        <input type="text" class="form-control" id="qs_nombre"  name="qs_nombre"  >
                    </div>
                    <div class="form-group">
                        <label>*E-mail:</label>
                        <input type="email" class="form-control" id="qs_email" name="qs_email" >
                    </div>
                    <div class="form-group">
                        <label>*Teléfono:</label>
                        <input type="tel" class="form-control" id="qs_tel" name="qs_tel" >
                    </div>
                    <div class="form-group">
                        <label>*Queja/Sugerencia:</label>
                        <select class="form-control"  id="qs_slect" name="qs_slect">
                            <option value="">*Selecciona una opción</option>
                            <option value="Sugerencia">Sugerencia</option>
                            <option value="Queja">Queja</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>*Comentarios:</label>
                        <textarea class="form-control" id="qs_msj" name="qs_msj" rows="3" cols="40" ></textarea>
                    </div>
                    <input type="submit" name="qs_submit" class="button" id="qs_submit" value="Enviar">
                </form>

            </div>
        </div>

    </div>
</div>

<!-- Modal de formularios -->
<div class="modal fade" id="form_alert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Mensaje enviado</h1>
            </div>
            <div class="modal-body">
                <p class="alert_msg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo base_url('assets/js/contact.form.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-validate/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/megamenu.js'); ?>?v=1.0.1"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/overlay_service.js'); ?>"></script>
</body>
</html>
