<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
    <head>
        <!-- Basic -->
        <title>Lofton y Asociados <?php echo isset($title)? ' | ' . $title : '' ; ?></title>
        <!-- Define Charset -->
        <meta charset="utf-8">
        <!-- Responsive Metatag -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Page Description and Author -->
        <meta name="description" content="<?php echo isset($description)? $description : '' ; ?>">
        <meta name="keywords" content="<?php echo isset($keywords)? $keywords : '' ; ?>">
        <meta name="author" content="Lofton y Asociados">
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">
        <!-- Bootstrap CSS  -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css" type="text/css'); ?>" media="screen">
        <!-- Revolution Banner CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/settings.css'); ?>" media="screen" />
        <!-- CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom_style.css'); ?>?v=1.0.1" media="screen">
        <!-- Responsive CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>" media="screen">
        <!-- Css3 Transitions Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css'); ?>" media="screen">
        <!-- Color CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/green.css'); ?>" title="green" media="screen" />
        <!-- Fontello Icons CSS Styles  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fontello.css'); ?>" media="screen">
        <!--[if IE 7]><link rel="stylesheet" href="<?php echo base_url('assets/css/fontello-ie7.css'); ?>"><![endif]-->

        <!-- LOFTON JS  -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.migrate.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/modernizrr.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.fitvids.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/nivo-lightbox.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.appear.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/count-to.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.textillate.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.lettering.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easypiechart.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.nicescroll.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.parallax.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/mediaelement-and-player.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.themepunch.plugins.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.themepunch.revolution.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/script.js'); ?>"></script>

        <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <!-- Container -->
        <div id="container">
            <!-- Start Header -->
            <div class="hidden-header"></div>
            <header class="clearfix">
                <!-- Start Top Bar -->
                <div class="top-bar color-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Start Contact Info -->
                                <ul class="contact-details">
                                    <li><i class="icon-mobile-2"></i> <span id="contact_phone">+52 55 1500-0666</span></li>
                                    <li><a href="" data-toggle="modal" data-target="#quejasModal"><i class="icon-mail-2"></i> Quejas y sugerencias</a></li>
                                    <!-- li><a href="<?php echo base_url('#'); ?>"><i class="icon-mail-2"></i> <span class="__cf_email__" data-cfemail="cda4a3aba28db4a2b8bfaea2a0bdaca3b4e3aea2a0">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></li-->
                                </ul>
                                <!-- End Contact Info -->
                            </div>
                            <div class="col-md-6">
                                <!-- Start Social Links -->
                                <ul class="social-list">
                                    <li>
                                        <a class="facebook sh-tooltip" target="_blank" data-placement="bottom" title="Facebook" href="https://www.facebook.com/LoftonSC"><i class="icon-facebook-2"></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter sh-tooltip" target="_blank" data-placement="bottom" title="Twitter" href="https://twitter.com/Lofton_sc"><i class="icon-twitter-2"></i></a>
                                    </li>
                                    <li>
                                        <a class="google sh-tooltip" target="_blank" data-placement="bottom" title="YouTube" href="https://www.youtube.com/user/loftoncontadores/featured"><i class="icon-youtube-play"></i></a>
                                    </li>
                                    <li>
                                        <a class="google sh-tooltip" target="_blank" data-placement="bottom" title="Google Plus" href="https://plus.google.com/+Loftonsc/posts"><i class="icon-gplus"></i></a>
                                    </li>                                   
                                    <li>
                                        <a class="linkdin sh-tooltip" target="_blank" data-placement="bottom" title="Linkedin" href="https://www.linkedin.com/company/lofton-y-asociados-s-c-?trk=ppro_cprof"><i class="icon-linkedin"></i></a>
                                    </li>      
                                </ul>
                                <!-- End Social Links -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Top Bar -->

                <?php $this->load->view('layout/menu'); ?>

            </header>
            <!-- End Header -->
         
            <div id="body_page">
                

