<?php
        $url = "http://bbv.terra.com.mx/bancomerindicators/mainboard5.html";
        $str = file_get_contents($url);
        $str_value = strip_tags($str);
//IPC
        $IPC_ubicacion = strpos($str_value, 'IPC');
        $valorIPC = str_replace(',','',substr($str_value, ($IPC_ubicacion + 3), 9));
//DOLAR COMPRA
        $DOLARCOMPRA_ubicacion = strpos($str_value, 'COMPRA');
        $valorcompra = str_replace(',','',substr($str_value, ($DOLARCOMPRA_ubicacion + 8), 5));
//DOLAR VENTA
        $DOLARVENTA_ubicacion = strpos($str_value, 'VENTA');
        $valorventa = str_replace(',','',substr($str_value, ($DOLARVENTA_ubicacion + 7), 5));
//DOW JONES
        $DOWJONES_ubicacion = strpos($str_value, 'DOW');
        $valordow = str_replace(',','',substr($str_value, ($DOWJONES_ubicacion + 9), 9));

  
?>

<div class="container">
    <h1 class="text-center">Indicadores</h1> <br />

    <!-- Start Milestone Block 1 -->
    <div class="milestone-block" data-animation="fadeIn" data-animation-delay="01">
        <div class="milestone-icon"><i class="icon-dollar"></i></div>
        <div class="milestone-right">
            <div class="milestone-text">Dólar Compra</div>
            <div class="milestone-number">$<span class="qty"><?php echo $valorcompra; ?></span></div>

        </div>
    </div>
    <!-- End Milestone Block 1 -->

    <!-- Start Milestone Block 2 -->
    <div class="milestone-block" data-animation="fadeIn" data-animation-delay="02">
        <div class="milestone-icon"><i class="icon-money"></i></div>
        <div class="milestone-right">
            <div class="milestone-text">Dólar venta</div>
            <div class="milestone-number">$<span class="qty"><?php echo $valorventa; ?></span></div>

        </div>
    </div>
    <!-- End Milestone Block 2 -->

    <!-- Start Milestone Block 3 -->
    <div class="milestone-block" data-animation="fadeIn" data-animation-delay="03">
        <div class="milestone-icon"><i class="icon-chart"></i></div>
        <div class="milestone-right">
            <div class="milestone-text">IPC</div>
            <div class="milestone-number"><span class="qty"><?php echo $valorIPC; ?></span></div>

        </div>
    </div>
    <!-- End Milestone Block 3 -->

    <!-- Start Milestone Block 4 -->
    <div class="milestone-block" data-animation="fadeIn" data-animation-delay="04">
        <div class="milestone-icon"><img src="<?php echo base_url('assets/img/dow_jonesicon.png'); ?>" alt=""></div>
        <div class="milestone-right">
            <div class="milestone-text">Dow Jones </div>
            <div class="milestone-number"><span class="qty"><?php echo $valordow; ?></span></div>

        </div>
    </div>
    <!-- End Milestone Block 4 -->


</div>