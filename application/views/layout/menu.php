<!-- Start Header ( Logo & Naviagtion ) -->

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">  
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                <span class="sr-only">Menú</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/img/logo.jpg'); ?>" alt="LOFTON"></a>
            <img src="<?php echo base_url('assets/img/iso.jpg'); ?>" alt="ISO 9001"  class="iso" >           
        </div>

        <div class="collapse navbar-collapse js-navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown mega-dropdown" id="services_menu">
                    <a href="<?php echo base_url('servicios.html'); ?>" class="dropdown-toggle" data-toggle="dropdown">SERVICIOS &nbsp;<i class="icon-down-open-1 control-icon"></i> </a>
                    <ul class="dropdown-menu mega-dropdown-menu" >
                        
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/contabilidad.html'); ?>">Contabilidad</a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/contabilidad/contabilidad-general.html'); ?>">Contabilidad General</a></li>
                                <li><a href="<?php echo base_url('servicios/contabilidad/asesoria-financiera.html'); ?>">Asesoría Financiera</a></li>
                                <li><a href="<?php echo base_url('servicios/contabilidad/asesoria-fiscal.html'); ?>">Asesoría Fiscal</a></li>
                                <li><a href="<?php echo base_url('servicios/contabilidad/asesoria-financiera.html'); ?>">Inventarios</a></li>                        
                            </ul>    

                        </li>
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/auditoria.html'); ?>">Auditoria </a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/auditoria/auditoria-fiscal.html'); ?>">Auditoria Fiscal</a></li>
                                <li><a href="<?php echo base_url('servicios/auditoria/auditoria-financiera.html'); ?>">Auditoría Financiera</a></li>
                                <li><a href="<?php echo base_url('servicios/auditoria/auditoria-ante-imss-e-infonavit.html'); ?>">Auditoría ante IMSS e Infonavit</a></li>
                                <li><a href="<?php echo base_url('servicios/auditoria/auditoria-en-contribuciones-locales.html'); ?>">Auditoría de Contribuciones Locales</a></li>                                        
                                <li><a href="<?php echo base_url('servicios/auditoria/auditoria-operativa.html'); ?>">Auditoría Operativa </a></li>
                                <li class="level2"><a href="<?php echo base_url('servicios/auditoria/trabajos-especiales.html'); ?>">Auditoría Interna&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/auditoria/trabajos-especiales/dictamen-enajenacion.html'); ?>">Dictamen de Enajenación de Acciones</a></li>
                                        <li><a href="<?php echo base_url('servicios/auditoria/trabajos-especiales/devolucion-saldo.html'); ?>">Devolución de Saldos a Favor</a></li>
                                        <li><a href="<?php echo base_url('servicios/auditoria/trabajos-especiales/revision-rubros.html'); ?>">Revisión de Rubros Específicos </a></li>
                                        <li><a href="<?php echo base_url('servicios/auditoria/trabajos-especiales/auditoria-forense.html'); ?>">Auditoría Forense</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/nominas.html'); ?>">Nóminas </a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/nominas/administracion-de-nomina.html'); ?>">Administración de Nómina</a></li>                                
                                <li><a href="<?php echo base_url('servicios/nominas/administracion-de-seguro-social.html'); ?>">Administración de Seguro Social</a></li>
                                <li><a href="<?php echo base_url('servicios/nominas/maquila-de-nomina.html'); ?>">Maquila de Nómina</a></li>
                            </ul>
                        </li>


                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/juridico.html'); ?>">Jurídico</a>
                            <ul>
                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-civil.html'); ?>">Jurídico Civil &nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-civil/juicios-de-arrendamiento.html'); ?>">Juicios de Arrendamiento</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-civil/contratos-civiles.html'); ?>">Contratos Civiles</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-civil/especial-hipotecario.html'); ?>">Especial Hipotecario</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-civil/juicios-civiles.html'); ?>">Juicios Civiles</a></li>
                                    </ul>
                                </li>

                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-corporativo.html'); ?>">Jurídico Corporativo&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-corporativo/derecho-corporativo.html'); ?>">Derecho Corporativo</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-corporativo/propiedad-industrial.html'); ?>">Propiedad Industrial</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-corporativo/derecho-migratorio.html'); ?>">Derecho Migratorio</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-corporativo/gestoria-de-negocios.html'); ?>">Gestoría de Negocios</a></li>
                                    </ul>
                                </li>

                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-familiar.html'); ?>">Jurídico Familiar&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-familiar/divorcios.html'); ?>">Divorcios</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-familiar/controversia-familiar.html'); ?>">Controversia Familiar</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-familiar/interdiccion.html'); ?>">Interdicción</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-familiar/herencia.html'); ?>">Herencia</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-familiar/reconocimiento-de-paternidad.html'); ?>">Reconocimiento de Paternidad</a></li>
                                    </ul>
                                </li>

                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-fiscal.html'); ?>">Jurídico Fiscal&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/juicio-contencioso.html'); ?>">Juicio Contencioso </a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/recursos-de-revocacion-e-inconformidad.html'); ?>">Recurso de Revocación e Inconformidad </a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/amparo-fiscal.html'); ?>">Amparo Fiscal</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/gestion-fiscal-administrativa.html'); ?>">Gestión Fiscal y/o Administrativa </a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/acuerdos-conclusivos.html'); ?>">Acuerdos Conclusivos </a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/quejas-ante-la-prodecon.html'); ?>">Quejas ante la PRODECON </a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-fiscal/procedimientos-de-verificacion.html'); ?>">Procedimientos de Verificación Administrativa  </a></li>
                                    </ul>
                                </li>

                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-laboral.html'); ?>">Jurídico Laboral&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-laboral/auditoria-laboral.html'); ?>">Auditoría Laboral</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-laboral/despido-justificado.html'); ?>">Terminación Laboral</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-laboral/diligencias.html'); ?>">Gestión de Trámites Laborales</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-laboral/representacion-juridica.html'); ?>">Evaluación de Riesgos Laborales</a></li>
                                    </ul>
                                </li>

                                <li class="level2">
                                    <a href="<?php echo base_url('servicios/juridico/juridico-mercantil.html'); ?>">Jurídico Mercantil&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                                    <ul class="level3">
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-mercantil/cobranza.html'); ?>">Cobranza</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-mercantil/juicios-mercantiles.html'); ?>">Juicios Mercantiles</a></li>
                                        <li><a href="<?php echo base_url('servicios/juridico/juridico-mercantil/contratos-mercantiles.html'); ?>">Contratos Mercantiles</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li style="clear: both"></li>
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/consultoria-de-negocios.html'); ?>">Consultoría de Negocios</a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/consultoria-de-negocios/reingenieria-de-procesos.html'); ?>">Reingeniería de Procesos</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-de-negocios/reingenieria-organizacional.html'); ?>">Reingeniería Organizacional</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-de-negocios/erps-sistemas-de-informacion.html'); ?>">ERP'S - Sistemas de Información</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-de-negocios/cambio-estrategico.html'); ?>">Cambio Estratégico</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-de-negocios/cadena-de-suministro.html'); ?>">Cadena de Suministro</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/recursos-humanos.html'); ?>">Recursos Humanos</a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/recursos-humanos/atraccion-de-talento.html'); ?>">Atracción de Talento</a></li>
                                <li><a href="<?php echo base_url('servicios/recursos-humanos/estudios-sociolaborales.html'); ?>">Estudios Sociolaborales</a></li>
                                <li><a href="<?php echo base_url('servicios/recursos-humanos/evaluacion-psicometrica-por-competencia.html'); ?>">Evaluación Psicométrica por Competencia</a></li>
                            </ul>
                        </li>

                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/tecnologias-de-la-informacion.html'); ?>">Tecnologías de la Información</a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/tecnologias-de-la-informacion/hosting.html'); ?>">Hosting</a></li>
                                <li><a href="<?php echo base_url('servicios/tecnologias-de-la-informacion/servicios-de-ti.html'); ?>">Servicios de TI</a></li>
                                <li><a href="<?php echo base_url('servicios/tecnologias-de-la-informacion/servicios-de-consultoria.html'); ?>">Servicios de consultoría</a></li>
                                <li><a href="<?php echo base_url('servicios/tecnologias-de-la-informacion/help-desk.html'); ?>">Help Desk</a></li>
                                <li><a href="<?php echo base_url('servicios/tecnologias-de-la-informacion/infraestructura.html'); ?>">Infraestructura</a></li>
                            </ul>
                        </li> 

                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/consultoria-en-mercadotecnia.html'); ?>">Consultoría en Mercadotecnia</a>
                            <ul>
                                <li><a href="<?php echo base_url('servicios/consultoria-en-mercadotecnia/marketing_digital.html'); ?>">Marketing Digital</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-en-mercadotecnia/produccion_edicion_video.html'); ?>">Producción y Edición de Video</a></li>
                                <li><a href="<?php echo base_url('servicios/consultoria-en-mercadotecnia/diseno.html'); ?>">Diseño</a></li>
                                <!-- li><a href="<?php //echo base_url('servicios/consultoria-en-mercadotecnia/telemarketing.html');   ?>">Telemarketing</a></li-->
                            </ul>
                        </li>                              
                        <li class="col-sm-3">
                            <a class="dropdown-header" href="<?php echo base_url('servicios/fianzas-y-seguros.html'); ?>">Fianzas y Seguros</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="<?php echo base_url('acerca-de-lofton/conocenos.html'); ?>" class="dropdown-toggle" data-toggle="dropdown" >ACERCA DE LOFTON&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('acerca-de-lofton/conocenos.html'); ?>">Conócenos</a></li>
                        <li><a href="<?php echo base_url('acerca-de-lofton/historia.html'); ?>">Historia</a></li>
                        <li><a href="<?php echo base_url('acerca-de-lofton/por-que-lofton.html'); ?>">Por qué Lofton</a></li>
                        <li><a href="<?php echo base_url('acerca-de-lofton/problemas-dificiles.html'); ?>">Problemas Difíciles</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?php echo base_url('carrera.html'); ?>" class="dropdown-toggle" data-toggle="dropdown" >CARRERA&nbsp;<i class="icon-down-open-1 control-icon"></i></a>
                    <ul class="dropdown-menu" >
                        <li><a href="<?php echo base_url('carrera/plan-de-carrera.html'); ?>">Plan de Carrera</a></li>
                        <li><a href="<?php echo base_url('carrera/testimoniales.html'); ?>">Testimoniales</a></li>
                        <li><a href="<?php echo base_url('carrera/vacantes.html'); ?>">Vacantes</a></li>
                        <li><a href="<?php echo base_url('carrera/sube-tu-cv.html'); ?>">Sube tu CV</a></li>
                        <li><a href="http://www.loftonsc.com/enlacelofton/" target="_blank">Enlace LOFTON</a></li>                        
                    </ul>
                </li>
                <li><a href="<?php echo base_url('contacto.html'); ?>">CONTACTO</a></li>
                <li><a href="http://www.loftonsc.com/blog/">BLOG</a></li>
            </ul>

        </div>
        <!-- /.nav-collapse -->
    </div>
</nav>


<!-- End Header ( Logo & Naviagtion ) -->