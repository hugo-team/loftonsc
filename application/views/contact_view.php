<!-- Start Map -->
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3762.445894863817!2d-99.162415!3d19.436333!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x37228cd3b04bde54!2sLofton+y+Asociados!5e0!3m2!1ses-419!2sus!4v1495061676592" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
<!-- End Map -->

<!-- Start Content -->
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Contactanos</span></h4>
                <!-- Start Contact Form -->
                <div id="contact-form" class="contatct-form">
                    <div class="loader"></div>
                    <form class="contactForm" id="cform" name="cform">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Nombre<span class="required">*</span></label>
                                <span class="name-missing">Ingresa tu nombre</span>  
                                <input id="name" name="name" type="text" value="" size="30">
                            </div>
                            <div class="col-md-6">
                                <label for="e-mail">Email<span class="required">*</span></label>
                                <span class="email-missing">Ingresa un e-mail válido</span> 
                                <input id="e-mail" name="e-mail" type="text" value="" size="30">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="message">Comentarios</label>
                                <span class="message-missing">Escribe un comentario</span>
                                <textarea id="message" name="message" cols="45" rows="10"></textarea>
                                <input type="submit" name="enviar" class="button" id="enviar" value="Enviar">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Contact Form -->

            </div>

            <div class="col-md-4">

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Información de contacto</span></h4>

                <!-- Some Info -->
                <p>No dudes en ponerte en contacto con nosotros si necesitas asesoría o tienes dudas respecto a nuestros servicios. </p>
                <p>O bien, te invitamos a dejar tus comentarios y opiniones, ya que éstas son esenciales para la mejora continua de nuestra empresa.</p>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:10px;"></div>

                <!-- Info - Icons List -->
                <ul class="icons-list">
                    <li><i class="icon-location-1"></i> Guillermo Prieto #76, Col. San Rafael C.P. 06470, México D.F.</li>
                    <li><i class="icon-mail-1"></i> <a class="" href="javascript:void(0)" >contacto@loftonsc.com</a></li>
                    <li><i class="icon-mobile-1"></i>  +52 55 1500-0666</li>
                </ul>

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:15px;"></div>

                <!-- Classic Heading -->
                <h4 class="classic-title"><span>Horario de atención</span></h4>

                <!-- Info - List -->
                <ul class="list-unstyled">
                    <li><i class="icon icon-time"></i> <strong>Lunes - Viernes</strong> - 9:00 am a 19:00 pm</li>
                    <li><i class="icon icon-time"></i> <strong>Sábado - Domingo</strong> - Cerrado</li>
                </ul>

            </div>

        </div>

    </div>
</div>
<!-- End content -->
