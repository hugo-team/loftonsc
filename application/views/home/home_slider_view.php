<!-- START REVOLUTION SLIDER 3.1 rev5 fullwidth mode -->
<div class="fullwidthbanner-container">
    <div class="fullwidthbanner" >
        <ul>
            <!-- SLIDE 1 -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url('assets/img/slide-01-bg.jpg'); ?>" data-fullwidthcentering="on" alt="slidebg2"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <!-- LAYER NR. 1 -->
                <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                     data-x="500"
                     data-y="125"

                     data-speed="300"
                     data-start="1600"
                     data-easing="easeOutExpo"><span class="accent-color">LOFTON Y ASOCIADOS</span><br>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfl start"
                     data-x="500"
                     data-y="195"

                     data-speed="300"
                     data-start="1900"
                     data-easing="easeOutExpo">Es una Firma de Consultoría con más de 18 años de experiencia.
                   
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfl start"
                     data-x="500"
                     data-y="247"                   
                     data-speed="300"
                     data-start="2200"
                     data-easing="easeOutExpo">
                     Maximizamos el crecimiento de las empresas al incrementar su <br> 
                    rentabilidad, competitividad y éxito.
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption sfb start"
                     data-x="840"
                     data-y="314"
                     data-speed="300"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    <a href="<?php echo base_url('acerca-de-lofton/conocenos.html'); ?>" class="tp-caption main-button ">Conoce más</a>
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sfl start"
                     data-x="left"
                     data-y="bottom"
                     data-speed="1000"
                     data-start="1000"
                     data-easing="Power1.easeOut">
                    <img src="<?php echo base_url('assets/img/slide-02-image-01.png'); ?>" alt="" />
                </div>

            </li>
            <!-- SLIDE 2 -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url('assets/img/slide-02-bg.jpg'); ?>" data-fullwidthcentering="on" alt="slidebg2"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <!-- LAYER NR. 1 -->
                <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                     data-x="440"
                     data-y="125"
                     data-speed="300"
                     data-start="1600"
                     data-easing="easeOutExpo"><span class="accent-color">¿POR QUÉ LOFTON?</span><br>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfl start"
                     data-x="440"
                     data-y="195"
                     data-speed="300"
                     data-start="1900"
                     data-easing="easeOutExpo">
                    La atención personalizada de nuestros especialistas ofrece<br />
                    la solución eficaz a los problemas empresariales. 
                    
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfr start"
                     data-x="440"
                     data-y="260"
                     data-speed="300"
                     data-start="2200"
                     data-easing="easeOutExpo"> 
                    Siempre buscamos los estándares más altos de calidad.
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption sfb start"
                     data-x="840"
                     data-y="314"
                     data-speed="300"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    <a href="<?php echo base_url('acerca-de-lofton/por-que-lofton.html'); ?>" class="tp-caption main-button ">Conoce más</a>
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sfl start"
                     data-x="50"
                     data-y="bottom"
                     data-speed="1000"
                     data-start="1000"
                     data-easing="Power1.easeOut">
                    <img src="<?php echo base_url('assets/img/slide-03-image-01.png'); ?>" alt="" />
                </div>
            </li>

            <!-- SLIDE 3  -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url('assets/img/slide-05-bg.jpg'); ?>" data-fullwidthcentering="on" alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <!-- LAYER NR. 1 -->
                <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                     data-x="center"
                     data-y="110"

                     data-speed="300"
                     data-start="1000"
                     data-easing="easeOutExpo"><span class="accent-color">REFORMA LABORAL 2017</span><br>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfb start"
                     data-x="center"
                     data-y="182"

                     data-speed="300"
                     data-start="1300"
                     data-easing="easeOutExpo">
                        Entérate de las reformas aprobadas en materia <br />laboral para 2017.
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption small_font_size light_font_weight gray_font_color text-center sfb start"
                     data-x="center"
                     data-y="220"

                     data-speed="300"
                     data-start="1600"
                     data-easing="easeOutExpo"><br />
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption sfr start"
                     data-x="center"
                     data-y="300"
                     data-hoffset="84"

                     data-speed="600"
                     data-start="2000"
                     data-easing="easeOutExpo">
                    <a href="http://www.loftonsc.com/blog/reforma-laboral-2017/" class="tp-caption main-button ">Conoce más</a> 
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sfl start"
                     data-x="center"
                     data-y="292"
                     data-hoffset="-84"

                     data-speed="600"
                     data-start="2000"
                     data-easing="easeOutExpo">
                </div>
            </li>

            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url('assets/img/slide-04-bg.jpg'); ?>" data-fullwidthcentering="on" alt="slidebg2"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <!-- LAYER NR. 1 -->
                <div class="tp-caption uppercase big_font_size boldest_font_weight dark_font_color sft start"
                     data-x="380"
                     data-y="120"
                     data-speed="300"
                     data-start="1600"
                     data-easing="easeOutExpo"><span class="accent-color">¿ESTÁS CUMPLIENDO CON <br/>TUS OBLIGACIONES FISCALES?</span><br>
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_font_size regular_font_weight dark_font_color sfl start"
                     data-x="500"
                     data-y="215"
                     data-speed="300"
                     data-start="1900"
                     data-easing="easeOutExpo">
                    Averígualo con el Dictamen Fiscal 2016. <br />Fecha límite de entrega 15 de Julio.
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption mini_font_size light_font_weight gray_font_color sfr start"
                     data-x="700"
                     data-y="250"

                     data-speed="300"
                     data-start="2200"
                     data-easing="easeOutExpo">
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption sfb start"
                     data-x="540"
                     data-y="314"

                     data-speed="300"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    <a href="<?php echo base_url('servicios/auditoria/auditoria-fiscal.html'); ?>" class="tp-caption  main-button ">Conoce más</a>
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sfl start"
                     data-x="rigth"
                     data-y="top"

                     data-speed="1000"
                     data-start="1000"
                     data-easing="Power1.easeOut"><img src="<?php echo base_url('assets/img/slide-04-image-01.png'); ?>" alt="" />
                </div>
            </li>

                        
        </ul>
        <div class="tp-bannertimer" style="visibility:hidden;"></div>
    </div>
</div>


<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function () {
        revapi = jQuery('.fullwidthbanner').revolution({

            delay: 9000,
            startwidth: 1140,
            startheight: 450,
            hideThumbs: 200,

            thumbWidth: 100,
            thumbHeight: 50,
            thumbAmount: 3,

            navigationType: "none",
            navigationArrows: "solo",
            navigationStyle: "round",

            touchenabled: "on",
            onHoverStop: "on",

            navigationHAlign: "center",
            navigationVAlign: "bottom",
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: "on",
            fullScreen: "off",
            lazyLoad: "on",

            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: "off",

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
        });
    });
</script>