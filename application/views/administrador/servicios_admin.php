<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Loftonsc | Administrador</title>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">
        <!-- Bootstrap -->
        <link href="<?php echo site_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
        <?php foreach ($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php foreach ($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
    </head>
    <body>
        
        <div class="container-fluid">
            <a href='<?php echo site_url('administrador/services_management') ?>'>Servicios</a> |
            <a href='<?php echo site_url('administrador/subservices_management') ?>'>Sub servicios</a> |
            <a href='<?php echo site_url('administrador/subsubservices_management') ?>'>Sub sub servicios</a> |
            <a href='<?php echo site_url('administrador/managers_management') ?>'>Gerentes</a> |
            <!--a href='<?php echo site_url('administrador/vacancy_management') ?>'>Vacantes</a-->
            <!--a href='<?php //echo site_url('administrador/') ?>'>Carrera</a -->
            <a href='<?php echo site_url('administrador/meta_magnament') ?>'>Metas</a> |
        </div>
        <div style='height:20px;'></div>  
        <div class="container-fluid">
            <?php echo $output; ?>
        </div>
         <script src="<?php echo site_url('assets/js/bootstrap.js'); ?>"></script>
    </body>
</html>
