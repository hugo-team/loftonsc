<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5" >
    <div class="container-fluid ">
        <h3>¿Necesitas más información?</h3><br /> 
        <form name="service_contact" id="service_contact">
            <div class="form-group">
                <input type="text" class="form-control "  id="nombre_completo" name="nombre_completo" placeholder="* Nombre Completo" >
            </div>
            <div class="form-group">
                <input type="email" class="form-control " id="mail" name="mail" placeholder="* Correo Electrónico" >
            </div>
            <div class="form-group">
                <input type="tel" class="form-control "  id="telefono" name="telefono" placeholder="Teléfono" >
            </div>
            <div class="form-group">
                <?php if (sizeof($services) > 0): ?>
                    <select class="form-control field" id="servicio_solicitado" name="servicio_solicitado"  >
                        <option value="" disabled="" selected="">Servicio Solicitado</option>
                        <?php foreach ($services as $service): ?>
                            <option value="<?php echo $service; ?>"><?php echo $service; ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php else : ?>
                    <input type="text" class="form-control " id="servicio_solicitado" name="servicio_solicitado" placeholder="* Servicio Solicitado" >
                <?php endif; ?>
            </div>
            <button id="enviar" type="submit" class="btn contact_sbmt">¡Contáctanos!</button>
        </form> 
    </div>
</div>
