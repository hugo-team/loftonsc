<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/services.css'); ?>?v=1.0.1" media="screen">

    <div  style="margin-top: 18px;">
        <div class="banner_service"  >
            <div class="service_tittle" style="<?php echo strlen($nombre_servicio) <= 15 ? 'min-width:25%' : strlen($nombre_servicio) > 15 && strlen($nombre_servicio) < 25 ? 'min-width:35%' : 'min-width:40%'; ?>" >
                <h1 class="titulo_seccion"><?php echo $nombre_servicio; ?></h1>
            </div>  
            <img src="<?php echo base_url('assets/img/servicios/upload/banners/' . $img_banner); ?>" class="img-responsive"  alt="">   
        </div>       
    </div>

    <div class="bg_service " style="background: url(<?php echo base_url('assets/img/servicios/upload/buttons/sub/' . $img_fondo); ?>);">
        <div class="bg_service_blur" >	
            
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="description">
                    <?php echo $descripcion; ?>
                    <br>

                    <div class="col-xs-12 col-md-12">	            		
                        <h2>Siempre dispuestos a atenderte</h2><br />
                        <p class="text-left">                        
                            Para consultas o solicitudes con una respuesta personalizada puede contactar a los encargados de área:
                        </p>
                        <br />
                    </div>
                    <?php foreach ($gerentes as $gerente): ?>

                        <div class="col-xs-12 col-md-6">
                            <p class="text-left"><i class="icon-user-1"></i>&nbsp;&nbsp;&nbsp;<strong><?php echo $gerente['nombre']; ?></strong>
                                <br>
                                <i class="icon-briefcase-1"></i>&nbsp;&nbsp;&nbsp;<?php echo $gerente['puesto']; ?></p>
                        </div>                  
                    <?php endforeach; ?> 
                 
                    <div class="col-xs-12 col-md-6">
                        <p>
                            <b><i class="icon-phone-1"></i> +52 55 1500 0666</b>   
                        </p>
                    </div>
                </div>
            </div> 
            
                <?php $this->load->view('contacto_view.php',$form_services); ?>   
            
        </div>
    </div>
    <div class="container">	   
        <h3 class="text-center titulo_seccion">Te proporcionamos los siguientes servicios:</h3>
        <?php
        $cont = 2;
        for ($i = 0; $i < $num_subs; $i++):
            if ($cols == 8 && $cont == 2) {
                $cols = 4;
            } else if ($cols == 4 && $cont == 2) {
                $cols = 8;
            }
            $num_subs % 2 != 0 && ($i + 1) == $num_subs ? $cols = '8 col-md-offset-2 ' : $cols = $cols;
            ?>            
            <div class="<?php echo 'col-md-' . $cols; ?> col-xs-12 service">                  
                <div class="row service_image">
                    <div class="caption">
                        <div class="caption_text">
                            <h3><?php echo $subservicios[$i]['sub_subservicio']; ?></h3>
                            <p><?php echo $subservicios[$i]['descripcion_breve'] != null ? $subservicios[$i]['descripcion_breve'] : ''; ?></p>
                            <p><a title="Conocer más" class="label label-default" href="<?php echo base_url('servicios/' . $folder . '/' . $subservicios[$i]['link_detalle']); ?>">Conocer más</a></p>
                        </div>
                    </div>
                    <img class="img-res" src="<?php echo base_url('assets/img/servicios/upload/buttons/sub/' . $subservicios[$i]['img_subservicio']); ?>">
                    <div class="yellow_tittle">
                        <h4><?php echo $subservicios[$i]['sub_subservicio']; ?></h4>
                    </div>
                </div>
            </div>
            <?php
            $cont == 2 ? $cont = 1 : $cont++;
            if ($cols == 8) {
                $cols = 4;
            } else {
                $cols = 8;
            }
        endfor;
        ?>

    </div>
