<!-- Start Home Slider -->
<div id="slider">

    <?php $this->load->view('home/home_slider_view'); ?>

    <!-- THE SCRIPT INITIALISATION -->
    <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->    
</div>
<!-- End Home Slider -->


<!-- Start Full Width Sections Content -->
<div id="content" class="full-sections">

    <!-- Start Big Heading -->
    <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <br />
        <!--h1>Lo más reciente en <strong>nuestro blog</strong> </h1-->
    </div>
    <!-- End Big Heading -->

    <!-- Divider -->
    <div class="hr1" style="margin-bottom:25px;"></div>

    <!-- Start Recent Projects Carousel -->
    <div class="full-width-recent-projects">
        <div class="projects-carousel touch-carousel navigation-3">
            <?php foreach ($result as $row) : ?>              
                <!-- Start Project Item -->
                <div class="portfolio-item item">
                    <div class="portfolio-border">
                        <!-- Start Project Thumb -->
                        <div class="portfolio-thumb">
                            <a  href="<?php echo $row['post']->guid; ?>" target="_blank">
                                <div class="thumb-overlay"><i class="icon-link-1"></i></div>
                                <img alt="" src="<?php echo $row['image']->guid; ?>" style="height:250px; width: 400px;"/>
                            </a>
                        </div>
                        <!-- End Project Thumb -->
                        <!-- Start Project Details -->
                        <div class="portfolio-details">
                            <a href="<?php echo $row['post']->guid; ?>" target="_blank">
                                <h4><?php echo $row['post']->post_title; ?></h4>  
                                <span>Leer más</span>   
                            </a>
                        </div>
                        <!-- End Project Details -->
                    </div>
                </div>
                <!-- End Project Item -->
            <?php endforeach; ?>
        </div>
    </div>
    <!-- End Recent Projects Carousel -->

    <!-- Divider -->
    <div class="hr1" style="margin-bottom:35px;"></div>

    <!-- Link To Portfolio -->
    <div class="text-center"><a href="http://www.loftonsc.com/blog/" class="btn-system btn-large border-btn"><i class="icon-globe-2"></i> Ir al Blog</a></div>


    <!-- Start Full Width Section 3 -->
    <div class="section" style="padding-top:50px; padding-bottom:25px; border-top:0; border-bottom:0; background:#fff;">

        <?php $this->load->view('layout/indicators_view'); ?>
    </div>
    <!-- End Full Width Section 3 -->

    <!-- Start Full Width Section 4 -->
    <div class="section" style="padding-top:120px; padding-bottom:120px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#fff;">
        <div class="container">

            <!-- Start Video Section Content -->
            <div class="section-video-content text-center">

                <!-- Start Animations Text -->
                <h1 class="fittext wite-text uppercase tlt">Mantente informado con las noticias más actuales.</h1>
                <h2 class=" wite-text uppercase tlt">¡Suscríbete a nuestro boletín!</h2>
                <!-- End Animations Text -->

                <!-- Divider -->
                <div class="hr1" style="margin-bottom:32px;"></div>                          

                <form class="form-horizontal" name="subscribe" id="subscribe" >
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                            <input type="email" class="form-control transparent_input" name="suscriber_mail" id="suscriber_mail" placeholder="Escribe tu correo">                            
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn-system btn-large border-btn btn-wite" id="news_suscribe" name="news_suscribe" ><i class="icon-mail-2"></i> Suscribirme</button>
                        </div>
                    </div>

                </form>

            </div>
            <!-- End Section Content -->

            <!-- Start Video -->
            <video class="section-video" poster="<?php echo base_url('assets/img/video/poster.jpg'); ?>" autoplay loop preload="none">
                <!-- MP4 source must come first for iOS -->
                <source type="video/mp4" src="<?php echo base_url('assets/img/video/video.mp4'); ?>" />
                <!-- WebM for Firefox 4 and Opera -->
                <source type="video/webm" src="<?php echo base_url('assets/img/video/video.webm'); ?>" />
                <!-- OGG for Firefox 3 -->
                <source type="video/ogg" src="<?php echo base_url('assets/img/video/video.ogv'); ?>" />
                <!-- Fallback flash player for no-HTML5 browsers with JavaScript turned off -->
                <object type="application/x-shockwave-flash" data="<?php echo base_url('assets/img/video/flashmediaelement.swf'); ?>"> 		
                    <param name="movie" value="<?php echo base_url('assets/img/video/flashmediaelement.swf'); ?>" /> 
                    <param name="flashvars" value="controls=false&amp;file=<?php echo base_url('assets/img/video/flashmediaelement.mp4'); ?>" /> 		
                    <!-- Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed -->
                    <img src="<?php echo base_url('assets/img/video/poster.jpg'); ?>" alt="No Video Image" title="No video playback capabilities" />
                </object> 	
            </video>
            <script>$('.section-video').mediaelementplayer({loop: true});</script>
            <!-- End Video -->

            <!-- Start Video Section overlay -->
            <div class="section-overlay"></div>

        </div>
    </div>
    <!-- End Full Width Section 4 -->

</div>
<!-- End Full Width Sections Content -->