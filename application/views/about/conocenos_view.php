<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/services.css'); ?>" media="screen">
<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-xs-12">
            <h1>CONÓCENOS</h1>
            <p class="desc_oc">Lofton &amp; Asociados es una Firma de Contadores y Abogados cuya propuesta de valor está basada en brindar servicios contables integrales y multidisciplinarios de manera profesional y efectiva. CONÓCENOS</p>
        </div>
        <div class="separator"></div>   
        <div class="col-xs-12 col-md-3 col-lg-4">
            <p class="text-left">Somos una Firma de Consultores cuya propuesta de valor está basada en brindar servicios integrales y multidisciplinarios de manera profesional, innovadora y efectiva.</p>
            <br />
            <p class="text-left">Diseñamos <strong>Estrategias Inteligentes</strong> y las implementamos, tomando completa responsabilidad de su eficacia y resultado. Nuestra prioridad siempre está enfocada en los intereses de nuestros clientes.</p>
            <br />
            <p class="text-left">Conformados por más de 250 profesionales capacitados en diferentes áreas de especialidad, con lo que conseguimos brindar a nuestros clientes servicios individuales de  acuerdo a sus necesidades.</p>
        </div>
        <div class="col-xs-12 col-md-9 col-lg-8">
            <img class="img-responsive" alt="Lofton &amp; Asociados es una Firma de Contadores y Abogados cuya propuesta de valor está basada en brindar servicios contables integrales y multidisciplinarios de manera profesional y efectiva." src="<?php echo base_url('assets/img/acercadelofton/conocenos.jpg'); ?>">
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">

            <h2 class="titulo_seccion">Misión</h2>
            <p class="text-left">Mantenernos a la vanguardia en las prácticas profesionales. Buscamos altos estándares de calidad con un compromiso ante la sociedad, brindamos soluciones a nuestros Clientes con alto nivel técnico y recomendamos acciones oportunas para incrementar los índices de rentabilidad, competitividad y éxito.</p>
        </div>
        <div class="col-xs-12 col-md-6">
            <h2 class="titulo_seccion">Visión</h2>
            <p class="text-left">Ser una Firma reconocida en el mercado profesional de la asesoría, el mundo empresarial y de negocios en México.</p>
        </div>
        <div class="separator"></div>
        <div class="col-xs-12 col-md-6">
            <h2 class="titulo_seccion">Valores</h2>
            <p class="text-left">1) Atender los problemas de nuestros Clientes proporcionando soluciones inteligentes de acuerdo a sus necesidades, con la finalidad de ofrecer un servicio personalizado y humano.</p>
            <br />
            <p class="text-left">2) Ser sensibles y comprometidos hacia nuestro capital humano para su crecimiento personal y profesional a través de la honestidad, integridad, lealtad y trabajo en equipo.</p>
        </div>
        <div class="col-xs-12 col-md-6">
            <h2 class="titulo_seccion">Política de Calidad</h2>
            <p class="text-left">Lofton es una Firma de Servicios Profesionales orientados en la completa satisfacción de las necesidades de nuestros Clientes, a través de la calidad y eficiencia de nuestro Talento Humano. Nos basamos en la mejora continua de nuestros procesos y apego a la normatividad que nos rige.</p>
        </div>
    </div> 
    <div class="separator"></div><br /><br />
    <div class="row">
        <h2>Servicios Corporativos</h2>
        <?php foreach ($services as $service): ?>
            <div class="col-md-4 col-xs-12 service">            
                <div class="row service_image">
                    <div class="caption">
                        <div class="caption_text">
                            <h3><?php echo $service->nombre_servicio; ?></h3>                        
                            <p><a title="Conocer más sobre <?php echo $service->nombre_servicio; ?>" class="label label-default" href="<?php echo base_url('servicios/service/' . $service->clave_servicio); ?>">Conocer más</a></p>
                        </div>
                    </div>
                    <img class="img-responsive" src="<?php echo base_url('assets/img/servicios/upload/banners/' . $service->img_banner); ?>">
                    <div class="yellow_tittle">
                        <h4><?php echo $service->nombre_servicio; ?></h4>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="separator"></div>
    <div class="row">
        <div class="col-xs-12 col-md-9">
            <p class="text-left"><strong>Lofton y Asociados</strong> brinda oportunidades de crecimiento y desarrollo abriendo sus puertas a nuevos talentos como socios, convirtiéndose en piezas clave para el funcionamiento y procesos de mejora continua de la Firma.</p>
            <p class="text-left">Con más de 17 años de experiencia<strong> Lofton y Asociados</strong> es una de las Firmas líderes en su segmento, atendiendo de manera profesional las necesidades de empresas pequeñas y medianas, acompañándolas y asesorándolas para que logren su consolidación y continuo crecimiento.</p>
            <p class="text-left">En <strong>Lofton y Asociados</strong> tenemos la filosofía de convertir en aliados a nuestros clientes. </p>

        </div>
        <?php $this->load->view('about/about_links_view'); ?>
    </div>    
</div>
</div>