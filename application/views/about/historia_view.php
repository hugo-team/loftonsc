<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/services.css'); ?>" media="screen">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">HISTORIA</h1>
            <p class="desc_oc">En Lofton y Asociados las empresas pasan de ser clientes para convertirse en aliados de negocio con el respaldo de nuestra filosofía de trabajo. HISTORIA</p>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 col-lg-4">
            <p class="text-left">En el año 1998, en la Ciudad de México es fundada la Firma de Consultores Lofton &amp; Asociados por nuestro actual  Socio Director C.P.C. Raymundo Del Río Sánchez, quien ya contaba para entonces con una exitosa trayectoria en el despacho que fundó su padre, prestigiado contador y académico con más de 20 publicaciones, las cuales a la fecha sirven aún como referencia a jóvenes estudiantes de la carrera de Contaduría.</p>
            <br />
            <p class="text-left">Nuestro Director junto a un pequeño grupo de colegas establece una pequeña oficina en la colonia Cuauhtémoc, teniendo una meta muy clara en mente, forjar las bases para construir uno de los despachos de contabilidad más importantes del país.</p>
        </div>
        <div class="col-xs-12 col-md-9 col-lg-8">
            <img class="img-responsive" alt="En Lofton &amp; Asociados las empresas pasan de ser clientes para convertirse en aliados de negocio con el respaldo de nuestra filosofía de trabajo." src="<?php echo base_url('assets/img/acercadelofton/historia.jpg'); ?>">
        </div>

    </div>


    <div class="row">
        <!--I-->
        <div class="col-xs-12 col-md-9 col-lg-8">
            <br>
            <p class="text-left">Con el proyecto en desarrollo busca aliarse con empresas fuera de México para internacionalizar su operación, obteniendo el apoyo de un despacho de San Diego, California.</p>
             <br />
            <p class="text-left">Al poco tiempo <b>Lofton y Asociados</b> inicia operaciones con gran éxito, siendo su principal fortaleza de negocio la recomendación de sus propios clientes.</p>
             <br />
            <p class="text-left">Poco a poco jóvenes talentos se van sumando a la Firma. Es así como en el 2009 se incorporan dos nuevas Socias, lo que le permite fortalecerse con una nueva e innovadora visión de negocio.</p>
             <br />
            <p class="text-left">La amplia perspectiva de sus Socias y las crecientes necesidades de sus clientes impulsan a <strong>Lofton y Asociados</strong> a extender su portafolio de servicios, anexando las áreas de <strong>Auditoría, Contabilidad, Administración de Nóminas, Consultoría Fiscal, Asesoría Legal, Consultoría de Negocios, Consultoría en Mercadotecnia, Tecnologías de la Información y Recursos Humanos</strong>, al mismo tiempo  pacta estrategias de negocios con empresas consolidadas en el área de Fianzas y Seguros como la empresa <strong>Aktiva Consultores</strong>.</p>
             <br />
            <p class="text-left">En la actualidad<strong> Lofton y Asociados</strong> continúa su crecimiento, consolidándose como una Firma que crece en conjunto con sus clientes, que avanza a pasos firmes y seguros, enfocada a diversos mercados empresariales; trabajando de la mano con sus clientes para llevarlos a tasas de crecimiento y rentabilidad superiores a sus expectativas.</p>
             <br />
            <p class="text-left">En <strong>Lofton y Asociados</strong>, nuestros clientes crecen junto a nosotros como aliados resolviendo problemas y enfrentando los obstáculos que plantea el mercado actual.</p>

        </div>
<?php $this->load->view('about/about_links_view'); ?>
    </div>
</div>

</div>