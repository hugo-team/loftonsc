<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">PROBLEMAS DIFÍCILES</h1>
            <p class="desc_oc">En Lofton y Asociados la atención es personalizada, cada cliente recibe en la medida de sus necesidades y de acuerdo a la problemática que presenta. PROBLEMAS DIFÍCILES</p>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 col-lg-3">
          <p class="text-left"><strong>En Lofton y Asociados</strong> trabajamos para ofrecer a todos nuestros clientes un detallado análisis de la situación. </p>
          <p class="text-left">
Al contratar alguno de los servicios que ofrecemos, nuestros clientes reciben <strong>Asesoría Integral </strong>por parte de nuestros especialistas, quienes realizan una evaluación de las circunstancias actuales de su empresa detectando riesgos y midiendo impactos, con lo que serán capaces de presentar propuestas y alternativas para resolver problemas y maximizar los beneficios.</p>
<p class="text-left">
En<strong> Lofton y Asociados</strong> la atención es personalizada, por lo que cada cliente recibe la asesoría adecuada en la medida de sus necesidades y de acuerdo a la problemática que presenta. 
</p></div>
        <div class="col-xs-12 col-md-9 col-lg-9">
            <img class="img-responsive" alt="En Lofton &amp; Asociados la atención es personalizada, cada cliente recibe en la medida de sus necesidades y de acuerdo a la problemática que presenta." src="<?php echo base_url('assets/img/acercadelofton/problemas-dificiles.jpg'); ?>">
        </div>

    </div>


    <div class="row">
        <!--I-->
        <div class="col-xs-12 col-sm-8 col-lg-9">
            <br>
            <p class="text-left">Nuestros colaboradores identifican que la mejor forma de ayudar a nuestros clientes en la resolución eficaz de sus problemas es mediante el análisis de escenarios reales, la colaboración de ideas estratégicas, innovadoras e integrales, pero no sólo para solucionar el problema en sí, sino para mejorar los procesos de su empresa.</p>
            <br>
            <h2 class="titulo_seccion">"Problemas difíciles, Estrategias inteligentes"</h2>

        </div>

       <?php $this->load->view('about/about_links_view'); ?>
    </div>

</div>