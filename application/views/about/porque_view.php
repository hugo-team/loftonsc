<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titulo_seccion">POR QUÉ LOFTON</h1>
            <p class="desc_oc">Lofton y Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores POR QUÉ LOFTON</p>
        </div>

    </div>

    <div class="row">
        <p class="text-left">Lofton &amp; Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores, distinguiéndonos de la competencia principalmente por:</p>

        <br />
        <!-- Start Image Service Box 1 -->
        <div class="col-md-6 image-service-box">
            <img class="img-thumbnail" alt="Lofton &amp; Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores" src="<?php echo base_url('assets/img/acercadelofton/por-que-lofton-servicios-integrales.jpg'); ?>">
            <h4 class="green_text">Servicios integrales y multidisciplinarios</h4>
            <p>Amplio portafolio de servicios y alianzas estratégicas clave.</p>
        </div>
        <!-- End Image Service Box 1 -->

        <!-- Start Image Service Box 2 -->
        <div class="col-md-6 image-service-box">
            <img class="img-thumbnail" alt="Lofton &amp; Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores" src="<?php echo base_url('assets/img/acercadelofton/por-que-lofton-atencion-y-asesoria.jpg'); ?>">
            <h4 class="green_text">Atención y asesoría personalizada</h4>
            <p>Los clientes se convierten en socios estratégicos y nosotros en sus aliados de negocio.</p>
        </div>
        <!-- End Image Service Box 2 -->

        <!-- Start Image Service Box 3 -->
        <div class="col-md-6 image-service-box">
            <img class="img-thumbnail" alt="Lofton &amp; Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores" src="<?php echo base_url('assets/img/acercadelofton/por-que-lofton-metodologia.jpg'); ?>">
            <h4 class="green_text">Metodología de trabajo</h4>
            <p>Se enfoca en un análisis real y profundo de la situación, proponiendo estrategias que den solución de fondo a los problemas y retos que enfrentan los clientes. Al mismo tiempo nos anticipamos a futuras complicaciones realizando una planeación estratégicamente enfocada en las necesidades del cliente.
                Problemas difíciles, Estrategias inteligentes.</p>
        </div>
        <!-- End Image Service Box 3 -->

        <!-- Start Image Service Box 4 -->
        <div class="col-md-6 image-service-box">
            <img class="img-thumbnail" alt="Lofton &amp; Asociados es una Firma de Contadores y Abogados plenamente comprometidos con los intereses de sus clientes, asociados y colaboradores" src="<?php echo base_url('assets/img/acercadelofton/por-que-lofton-principios-eticos.jpg'); ?>">
            <h4 class="green_text">Principios Éticos</h4>
            <p>Somos una Firma cuyos principios éticos son la columna vertebral de sus procesos y estrategias. La honestidad, lealtad, profesionalismo, trabajo en equipo y actitud de servicio rigen el día a día en nuestra Firma.</p>
        </div>
        <!-- End Image Service Box 4 -->
        <div style=" width:100%; float:right;">
           <?php $this->load->view('about/about_links_view'); ?> 
        </div>
            
    </div>

</div>

