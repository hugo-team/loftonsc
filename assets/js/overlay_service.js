$(document).ready(function () {

    $('.service_image').hover(
        function(){
            $(this).find('.caption').slideDown(350); 
            $(this).find('.yellow_tittle').slideUp(350); 
            $(this).find('.caption .caption_text').fadeIn(700); 
        },
        function(){
            $(this).find('.caption').fadeOut(); 
            $(this).find('.yellow_tittle').slideDown(350); 
            $(this).find('.caption .caption_text').fadeOut(); 
        }
    ); 
    

});

 