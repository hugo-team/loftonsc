$(document).on('click', '.mega-dropdown', function (e) {
    e.stopPropagation();

});

$(document).ready(function () {

    $(window).resize(function () {
        var displayWidth = $(window).width();
        var menu_height = $(document).height();
        $('.mega-dropdown-menu').height(menu_height);

        if (displayWidth > 768) {            
            $(".dropdown").hover(
                    function () {
                        $('.dropdown-menu', this).stop().slideDown("fast", function () {
                            if ($(this).attr('class') === 'dropdown-menu mega-dropdown-menu') {
                                $('#body_page').addClass('blur');
                            }
                        });
                    },
                    function () {
                        $('.dropdown-menu', this).stop().slideUp("fast", function () {
                            if ($(this).attr('class') === 'dropdown-menu mega-dropdown-menu') {
                                $('#body_page').removeClass('blur');
                            }
                        });
                    });

            $('.dropdown a.dropdown-toggle').click(function () {
                window.location = $(this).attr('href');
            });

            $(".level2").hover(function (e) {    
                $(this).find(".icon-down-open-1").toggleClass('level2_opened');
                $(this).children("ul").stop(true, false).slideToggle(250);                        
                e.preventDefault();
            });
        }
    }).resize();

});