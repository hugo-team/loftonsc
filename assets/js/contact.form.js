function show_loader(sbmt_btn) {
    $('#' + sbmt_btn).attr('disabled', true);
    $('#body_page').addClass('blur');
    $('#loader').find('img').hide();
    $('#loader').css('background-image', 'none').show();
}

function hide_loader(sbmt_btn) {
    $('#body_page').removeClass('blur');
    $('#loader').find('img').show();
    $('#loader').hide();
    $('#' + sbmt_btn).attr('disabled', false);
}

$(document).ready(function () {
    //submission scripts
    $('.contactForm').submit(function () {
        //statements to validate the form	
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = document.getElementById('e-mail');
        if (!filter.test(email.value)) {
            $('.email-missing').css({'opacity': 1});
        } else {
            $('.email-missing').css({'opacity': 0});
        }
        if (document.cform.name.value == "") {
            $('.name-missing').css({'opacity': 1});
        } else {
            $('.name-missing').css({'opacity': 0});
        }
        if (document.cform.message.value == "") {
            $('.message-missing').css({'opacity': 1});
        } else {
            $('.message-missing').css({'opacity': 0});
        }
        if ((document.cform.name.value == "") || (!filter.test(email.value)) || (document.cform.message.value == "")) {
            return false;
        }

        if ((document.cform.name.value != "") && (filter.test(email.value)) && (document.cform.message.value != "")) {

            //show the loading bar
            show_loader('enviar');

            $.ajax({
                type: "post",
                url: "/contact/contact_lofton",
                cache: false,
                data: $('#cform').serialize(),
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    var header_msg = '';
                    if (obj.sent === true) {
                        $('#form_alert').find('.modal-content').addClass('modal-success');
                        header_msg = 'Mensaje enviado';
                    } else if (obj.sent === false) {
                        $('#form_alert').find('.modal-content').addClass('modal-error');
                        header_msg = 'Algo salió mal';
                    }
                    $('.modal-header h1').text(header_msg);
                    $('#form_alert').find('.alert_msg').html(obj.msg);
                    $('#form_alert').modal('show');

                    hide_loader('enviar');
                    document.getElementById("cform").reset();
                }
            });

            return false;
        }
    });

    //Services contatc form validation
    $('#service_contact').validate({
        rules: {
            nombre_completo: {required: true, minlength: 5},
            mail: {required: true, email: true},
            telefono: {required: true, number: true, minlength: 8, maxlength: 10},
            servicio_solicitado: {required: true}
        },
        messages: {
            nombre_completo: "Debe idicar su nombre.",
            mail: "Debe introducir un email válido.",
            telefono: "El número de teléfono introducido no es correcto.",
            servicio_solicitado: "Indique el servicio solicitado.",
        },
        submitHandler: function (form) {
            show_loader('enviar');
            $.ajax({
                type: "post",
                url: "/loftonsc/contact/contact_service",
                cache: false,
                data: $('#service_contact').serialize(),
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    var header_msg = '';
                    if (obj.sent === true) {
                        $('#form_alert').find('.modal-content').addClass('modal-success');
                        header_msg = 'Mensaje enviado';
                    } else if (obj.sent === false) {
                        $('#form_alert').find('.modal-content').addClass('modal-error');
                        header_msg = 'Algo salió mal';
                    }
                    $('.modal-header h1').text(header_msg);
                    $('#form_alert').find('.alert_msg').html(obj.msg);
                    $('#form_alert').modal('show');
                    hide_loader('enviar');

                    document.getElementById("service_contact").reset();
                }
            });

            return false;
        }
    });


    //Newsletter vaidation
    $('#subscribe').validate({
        rules: {
            suscriber_mail: {required: true, email: true}
        },
        messages: {
            suscriber_mail: "Debe introducir un email válido."
        },
        submitHandler: function (form) {
            show_loader('news_suscribe');
            $.ajax({
                type: "post",
                url: "/loftonsc/newsletter",
                cache: false,
                data: $('#subscribe').serialize(),
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    var header_msg = '';
                    if (obj.subscribed === true) {
                        $('#form_alert').find('.modal-content').addClass('modal-success');
                        header_msg = 'Suscripcion exitosa';
                    } else if (obj.subscribed === false) {
                        $('#form_alert').find('.modal-content').addClass('modal-error');
                        header_msg = 'Algo salió mal';
                    }
                    $('.modal-header h1').text(header_msg);
                    $('#form_alert').find('.alert_msg').html(obj.msg);                    
                    $('#form_alert').modal('show');
                    hide_loader('news_suscribe');
                    document.getElementById("subscribe").reset();
                }
            });

            return false;
        }
    });



    //Complaints and suggestions vaidation
    $('#qs').validate({
        rules: {
            qs_nombre: {required: true, minlength: 10},
            qs_email: {required: true, email: true},
            qs_tel: {required: true, number: true, minlength: 8, maxlength: 10},
            qs_msj: {required: true, minlength: 10},
            qs_slect: {required: true}
        },
        messages: {
            qs_nombre: "Debe introducir su nombre completo.",
            qs_email: "Debe introducir un email válido.",
            qs_tel: "Debe introducir un teléfono válido.",
            qs_msj: "Debe escribir un mensaje.",
            qs_slect: "Selecciona una opción."

        },
        submitHandler: function (form) {
            show_loader('qs_submit');
            $.ajax({
                type: "post",
                url: "/loftonsc/contact/suggestions",
                cache: false,
                data: $('#qs').serialize(),
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    var header_msg = '';
                    if (obj.sent === true) {
                        $('#form_alert').find('.modal-content').addClass('modal-success');
                        header_msg = 'Mensaje enviado.';
                    } else if (obj.sent === false) {
                        $('#form_alert').find('.modal-content').addClass('modal-error');
                        header_msg = 'Algo salió mal';
                    }
                    $('.modal-header h1').text(header_msg);
                    $('#form_alert').find('.alert_msg').html(obj.msg);
                    $('#form_alert').modal('show');
                    hide_loader('qs_submit');
                    document.getElementById("subscribe").reset();
                }
            });

            return false;
        }
    });


    //Upload CV vaidation
    $('#upload_cv').validate({
        rules: {
            leaflet_name: {required: true, minlength: 10},
            suscriber_mail: {required: true, email: true},
            leaflet_phone: {required: true, number: true, minlength: 8, maxlength: 10},
            leaflet_msg: {required: true, minlength: 10},
            leaflet_cv: {required: true, extension: "docx?|pdf", filesize: 3145728}
        },
        messages: {
            leaflet_name: "Debe introducir su nombre completo.",
            leaflet_mail: "Debe introducir un email válido.",
            leaflet_phone: "Debe introducir un teléfono válido.",
            leaflet_msg: "Debe escribir un mensaje.",
            leaflet_cv: "Adjuta tu CV."

        }
    });

    $('#upload_cv').submit(function () {
        if ($(this).valid()) {
            $('#send_cv').attr('disabled', true);
            $('#body_page').addClass('blur');
            $('#loader').find('img').hide();
            $('#loader').css('background-image', 'none').show();
        }
    });



    $("#form_alert").on("hidden.bs.modal", function () {
        $('#form_alert').find('.modal-content').removeClass('modal-success');
        $('#form_alert').find('.modal-content').removeClass('modal-error');
    });

});
