<?php
/**
 * The template for displaying the featured carousel on homepage.
 * Gets the category for the posts from the theme options. 
 * If no category is selected, displays the latest posts.
 *
 * @package  WellThemes
 * @file     feat-carousel.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 * 
 **/
?>
<?php
	$carousel_title = get_post_meta($post->ID, 'wt_meta_carousel_title', true);
	$cat_id = get_post_meta($post->ID, 'wt_meta_carousel_cat', true);	
	
	$args = array(
		'cat' => $cat_id,
		'post_status' => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page' => 15
	);
		
?>
<div id="feat-carousel" class="carousel-section" data-animation="fadeInRight">
	<script>
		jQuery(document).ready(function($) {				
			$(".carousel").show();
			$('.carousel').flexslider({
				slideshow: true,							
				slideshowSpeed: 3000,   
				mousewheel: false,
				keyboard: true,
				controlNav: false,
				directionNav: false,	
				animation: "slide",
				itemWidth: 260,
				itemMargin: 35,
				minItems: 1,                   
				maxItems: 4,                   
				move: 1,
			});
		});
	</script>				
				
		<div class="carousel">
			<ul class="slides">
				<?php $query = new WP_Query( $args ); ?>
					<?php if ( $query -> have_posts() ) : ?>
						<?php while ( $query -> have_posts() ) : $query -> the_post(); ?>
								<?php if ( has_post_thumbnail()) { ?>				
									<li>
										<div class="thumb overlay">
											<a href="<?php the_permalink(); ?>" >
												<?php the_post_thumbnail( 'wt260_145' ); ?>
											</a>								
										</div>
										<div class="post-info">									
											
											<div class="entry-meta">
												<span class="author">
													<?php _e('Author:', 'wellthemes'); ?>
													<?php the_author_posts_link(); ?> 
												</span>

												<span class="sep">-</span>
												
												<span class="date"><?php echo get_the_date(); ?></span>
																	
											</div>	
							
							
											<div class="title">
												<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
											</div>	
																																
										</div>										
									</li>							
							<?php } ?>
						<?php endwhile; ?>
					<?php endif;?>
				<?php wp_reset_query();?>				
			</ul>		
		</div>
</div>