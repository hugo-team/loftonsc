<?php
/**
 * The template for displaying the featured single categories section on homepage.
 * Gets the category for the posts from the theme options. 
 * If no category is selected, displays the latest posts.
 *
 * @package  WellThemes
 * @file     feat-section.php
 * @author   WellThemes Team
 * @link 	 http://wellthemes.com
 * 
 **/
?>
<div class="feat-section section">
	<?php
		
		$feat_sec1 = get_post_meta($post->ID, 'wt_meta_show_feat_sec1', true);
		$feat_sec2 = get_post_meta($post->ID, 'wt_meta_show_feat_sec2', true);	
		
		$sec1_title1 = get_post_meta($post->ID, 'wt_meta_sec1_title1', true);
		$sec1_title2 = get_post_meta($post->ID, 'wt_meta_sec1_title2', true);
	?>
		
		<div class="section-left sidetabs" data-animation="fadeInLeft">		

			<div class="tab-titles">
				<ul class="list">
					<li class="active"><a href="#tab1-content"><i class="fa fa-clock-o"></i></a></li>
					<li><a href="#tab2-content"><i class="fa fa-signal"></i></a></li>
					<li><a href="#tab3-content"><i class="fa fa-comments"></i></a></li>
				</ul>
			</div>
			
			<div class="tabs-content-container">
			
				<div id="tab1-content" class="tab-content" style="display: block;">	
					<?php 
						$cat_id = get_post_meta($post->ID, 'wt_meta_sec_left_cat', true);
						$num_posts = get_post_meta($post->ID, 'wt_meta_sec_left_posts', true);
						
						if (($num_posts < 1) or ($num_posts > 8)) {
							$num_posts = 8;
						}
						
						$args = array(
							'cat' => $cat_id,		
							'post_status' => 'publish',
							'ignore_sticky_posts' => 1,
							'posts_per_page' => $num_posts,
						);
									
					
						$query = new WP_Query( $args );
						if ( $query -> have_posts() ) :						
							while ( $query -> have_posts() ) : $query -> the_post(); ?>					
								<div class="item-post">
									<?php if ( has_post_thumbnail() ) {	?>
										<div class="thumb">
											<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt65_65' ); ?></a>
										</div>
									<?php }	?>
									<div class="post-right">										
										<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
										<div class="entry-meta">											
											<?php
												global $post;
												$comment_count = get_comments_number($post->ID);
												if ($comment_count > 0){ 
													$comment_class = ' main-color-bg';
												} else {
													$comment_class = ' no-comment-bg';
												}
												?>	
													<span class="comments-bg<?php echo $comment_class; ?>">
														<i class="fa fa-comment"></i>
														<?php comments_popup_link( __('0', 'wellthemes'), __( '1', 'wellthemes'), __('%', 'wellthemes')); ?>	
													</span>
													<span class="sep">-</span>
													
											<span class="date"><?php echo get_the_date(); ?></span>			
										</div>
																	
									</div>
								</div><?php
							endwhile;							
					endif;
					wp_reset_query(); ?>	
				</div>
				
				<div id="tab2-content" class="tab-content">
					<?php
						$args = array(
							'cat' => $cat_id,		
							'post_status' => 'publish',
							'ignore_sticky_posts' => 1,
							'posts_per_page' => $num_posts,
							'orderby' => 'comment_count'
						);									
					
						$query = new WP_Query( $args );
						if ( $query -> have_posts() ) :						
							while ( $query -> have_posts() ) : $query -> the_post(); ?>					
								<div class="item-post">
									<?php if ( has_post_thumbnail() ) {	?>
										<div class="thumb">
											<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt65_65' ); ?></a>
										</div>
									<?php }	?>
									<div class="post-right">										
										<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
										<div class="entry-meta">											
											<?php
												global $post;
												$comment_count = get_comments_number($post->ID);
												
												if ($comment_count > 0){ 
													$comment_class = ' main-color-bg';
												} else {
													$comment_class = ' no-comment-bg';
												}
												?>	
													<span class="comments-bg<?php echo $comment_class; ?>">
														<i class="fa fa-comment"></i>
														<?php comments_popup_link( __('0', 'wellthemes'), __( '1', 'wellthemes'), __('%', 'wellthemes')); ?>	
													</span>
													<span class="sep">-</span>													
											<span class="date"><?php echo get_the_date(); ?></span>			
										</div>
																	
									</div>
								</div><?php
							endwhile;							
					endif;
					wp_reset_query(); ?>	
				</div>
				
				<div id="tab3-content" class="tab-content">
					
						<?php 
							//get recent comments
							$args = array(
								   'status' => 'approve',
									'number' => $num_posts
								);	
							
							$postcount=0;
							$comments = get_comments($args);
							
							foreach($comments as $comment) :
									$postcount++;								
									$commentcontent = strip_tags($comment->comment_content);			
									if (strlen($commentcontent)> 50) {
										$commentcontent = mb_substr($commentcontent, 0, 49) . "...";
									}
									$commentauthor = $comment->comment_author;
									if (strlen($commentauthor)> 40) {
										$commentauthor = mb_substr($commentauthor, 0, 39) . "...";			
									}
									$commentid = $comment->comment_ID;
									$commenturl = get_comment_link($commentid); 
									$commentdate = get_comment_date( '', $commentid );
									
									?>
									
								   <div class="item-post">
										<div class="thumb">
											<?php echo get_avatar( $comment, '65' ); ?>
										</div>
										
										<div class="post-right">
											<div class="comment-author"><h5><?php echo $commentauthor; ?></h5></div>
											<div class="comment-text">
												<a<?php if($postcount==1) { ?> class="first"<?php } ?> href="<?php echo $commenturl; ?>"><?php echo $commentcontent; ?></a>
											</div>
											<div class="entry-meta">
												<span class="date">
													<?php echo $commentdate; ?>
												</span>
										
											</div>
										</div>
									</div>
						<?php endforeach; ?>
						<?php wp_reset_query();?>	
						
					
				</div>
			</div>		
		</div>		
		
		<div class="section-right" data-animation="fadeInRight">
			<?php
				if ($feat_sec1) { ?>
					<div class="section1">
						<div class="section-title">
							<h4><?php echo $sec1_title1; ?><span class="main-color"><?php echo $sec1_title2; ?></span></h4>
							<div id="sec1-nav" class="feat-sec-nav"></div>	
						</div>
					
						<div class="section-content">
							<?php
								$cat_id = get_post_meta($post->ID, 'wt_meta_sec1_cat', true);
								$post_ids = get_post_meta($post->ID, 'wt_meta_sec1_post_ids', true);
								$ids_array = '';
								$first_id = '';
								$orderby = 'date';
								
								if ($post_ids) {
									$ids_array = array_map( 'trim', explode( ',', $post_ids ) );
									$orderby = 'none';									
								}
								
							
							?>
							<div class="main-post">
								<?php
									
									$args = array(
										'cat' => $cat_id,
										'post__in' => $ids_array,											
										'post_status' => 'publish',
										'ignore_sticky_posts' => 1,
										'posts_per_page' => 1,
										'orderby' => $orderby
									);
									
									$query = new WP_Query( $args );
									if ( $query -> have_posts() ) :
										while ( $query -> have_posts() ) : $query -> the_post(); ?>
																		
											<?php if ( has_post_thumbnail() ) {	?>
												<div class="thumb overlay">
													<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt550_300' ); ?></a>
												</div>
											<?php }	?>
											
											<div class="entry-wrap">
												<div class="entry-meta">
													<span class="author">
														<?php _e('Author:', 'wellthemes'); ?>
														<?php the_author_posts_link(); ?> 
													</span>

													<span class="sep">-</span>
													
													<span class="date"><?php echo get_the_date(); ?></span>
													
													<?php 
														if ( wt_get_option( 'wt_enable_rating' ) == 1 ){
															ec_stars_rating_archive(); 
														}
													?>
			
												</div>	
	
												<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
												<p>
													<?php 
														$excerpt = get_the_excerpt();
														$trimmed_excerpt = wp_trim_words( $excerpt, 22);
														echo $trimmed_excerpt;
													?>
												</p>
											</div>
											
											<?php												
										endwhile;
									endif;
									wp_reset_query();
								?>
							</div>
														
							<div class="post-list list-slider">
								<ul class="slides">
									<?php
										$args = array(
											'cat' => $cat_id,
											'post__in' => $ids_array,											
											'post_status' => 'publish',
											'ignore_sticky_posts' => 1,
											'posts_per_page' => 10,
											'offset' => 1,
											'orderby' => $orderby
										);
										
										$i = 0;											
										$query = new WP_Query( $args );
										
										if ( $query -> have_posts() ) :
											while ( $query -> have_posts() ) : $query -> the_post();
												echo ($i % 2 === 0) ? "<li>" : null;
												$i++; ?>
												
												<div class="item-post">
													
													<?php if ( has_post_thumbnail() ) {	?>
														<div class="thumb overlay">
															<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt260_145' ); ?></a>
														</div>
													<?php }	?>
													
													<div class="entry-wrap">
														<div class="entry-meta">
															<span class="author">
																<?php _e('Author:', 'wellthemes'); ?>
																<?php the_author_posts_link(); ?> 
															</span>

															<span class="sep">-</span>														
															<span class="date"><?php echo get_the_date(); ?></span>			
														</div>	
			
														<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>													
													</div>
													
												</div>												
												<?php
												echo ($i % 2 === 0) ? "</li>" : null;	
											endwhile;
											echo ($i % 2 === 0) ? "</li>" : null;	
										endif;
										wp_reset_query(); 											
									?>
								</ul>
							</div>							
						</div>
					</div><!-- /section1 -->
					<?php 
				} //feat_sec1
								

				$sec2_title1 = get_post_meta($post->ID, 'wt_meta_sec2_title1', true);
				$sec2_title2 = get_post_meta($post->ID, 'wt_meta_sec2_title2', true);
		
				if ($feat_sec2) { ?>
					<div class="section2">
						<div class="section-title">
							<h4><?php echo $sec2_title1; ?><span class="main-color"><?php echo $sec2_title2; ?></span></h4>
							<div id="sec2-nav" class="feat-sec-nav"></div>	
						</div>
						
						<div class="section-content">
							<?php
								$cat_id = get_post_meta($post->ID, 'wt_meta_sec2_cat', true);
								$post_ids = get_post_meta($post->ID, 'wt_meta_sec2_post_ids', true);
								$ids_array = '';
								$first_id = '';
								$orderby = 'date';
								
								if ($post_ids) {
									$ids_array = array_map( 'trim', explode( ',', $post_ids ) );
									$orderby = 'none';									
								}
																	
								$args = array(
									'cat' => $cat_id,
									'post__in' => $ids_array,											
									'post_status' => 'publish',
									'ignore_sticky_posts' => 1,
									'posts_per_page' => 5,
									'orderby' => $orderby
								);
									
								$query = new WP_Query( $args );
								if ( $query -> have_posts() ) : ?>
									<div class="sec-slider">
										<ul class="list slides"><?php
											while ( $query -> have_posts() ) : $query -> the_post(); ?>
												<li>					
													<?php if ( has_post_thumbnail() ) {	?>												
														<div class="thumb overlay">
															<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt450_250' ); ?></a>
														</div>													
													<?php }	?>
													<div class="post-right">
														<div class="entry-meta">
															<span class="author">
																<?php _e('Author:', 'wellthemes'); ?>
																<?php the_author_posts_link(); ?> 
															</span>

															<span class="sep">-</span>
															
															<span class="date"><?php echo get_the_date(); ?></span>
																														
															<?php 
																if ( wt_get_option( 'wt_enable_rating' ) == 1 ){
																	ec_stars_rating_archive(); 
																}
															?>					
														</div>	
			
														<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
														<p>
															<?php 
																$excerpt = get_the_excerpt();
																$trimmed_excerpt = wp_trim_words( $excerpt, 20);
																echo $trimmed_excerpt;
															?>
														</p>
														<div class="excerpt-footer">
															
															<div class="footer-left">
																<div class="entry-meta">
																	<?php
																		global $post;
																		$comment_count = get_comments_number($post->ID);
																		if ($comment_count > 0){ 
																			$comment_class = ' main-color-bg';
																		} else {
																			$comment_class = ' no-comment-bg';
																		}
																	?>	
																	<span class="comments-bg<?php echo $comment_class; ?>">	
																		<i class="fa fa-comment"></i>
																		<?php comments_popup_link( __('0', 'wellthemes'), __( '1', 'wellthemes'), __('%', 'wellthemes')); ?>	
																	</span>																			
																</div>
																<div class="more-link">
																	<a href="<?php the_permalink() ?>"><?php _e('Read more', 'wellthemes'); ?></a>
																</div>
															</div>
															
															<div class="share-links">
																<span class="share"><?php _e('Share', 'wellthemes'); ?></span>
																<span class="links">
																	<a class="twitter" href="http://twitter.com/home?status=<?php echo urlencode( get_the_title() ); ?>%20<?php echo urlencode( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-twitter-square"></i><?php _e('Twitter', 'wellthemes'); ?></a>	
																	<a  class="fb" href="http://facebook.com/share.php?u=<?php echo urlencode( get_the_permalink() ); ?>&amp;t=<?php echo urlencode( get_the_title() ); ?>" target="_blank"><i class="fa fa-facebook-square"></i><?php _e('Facebook', 'wellthemes'); ?></a>
																</span>
															</div>
															
														</div>
														
													</div>
												</li>												
											<?php
											endwhile; ?>
										</ul>
									</div><?php
								endif;
								wp_reset_query();
								?>						
						</div>
		
					</div><!-- /section2 -->
				<?php } //feat_sec2 ?>
			
		</div><!-- /section-right -->
		
</div><!-- /feat-cat -->