<?php
/**
 * The template for displaying the featured slider on homepage.
 * Gets the category for the posts from the theme options. 
 * If no category is selected, displays the latest posts.
 *
 * @package  WellThemes
 * @file     feat-postlist.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 * 
 **/
?>
<div id="feat-postlist">
	<?php
		$title1 = get_post_meta($post->ID, 'wt_meta_postlist_title1', true);
		$title2 = get_post_meta($post->ID, 'wt_meta_postlist_title2', true);

		$cat_id = get_post_meta($post->ID, 'wt_meta_postlist_cat', true);
	
		if (!empty($title1) OR !empty($title2)){ ?>
			<div class="section-title">
				<h4><?php echo $title1; ?><span class="main-color"><?php echo $title2; ?></span></h4>
			</div>
	<?php }	?>
	
		<div class="archive-postlist">
			<?php			
				if ( get_query_var('paged') ) {
					$paged = get_query_var('paged');
				} elseif ( get_query_var('page') ) {
					$paged = get_query_var('page');
				} else {
					$paged = 1;
				}
					
				$args = array(
					'cat' => $cat_id,
					'post_status' => 'publish',
					'ignore_sticky_posts' => 1,
					 'paged' => $paged
				);			
				
				$i = 0;
				$wp_query = new WP_Query( $args );
				if ( $wp_query -> have_posts() ) :
					while ( $wp_query -> have_posts() ) : $wp_query -> the_post();
						get_template_part( 'content', 'excerpt' );
					endwhile;
				endif;
			?>
		</div>
	<?php wt_pagination(); ?>
	<?php wp_reset_query();?>
</div><!-- /feat-postlist -->