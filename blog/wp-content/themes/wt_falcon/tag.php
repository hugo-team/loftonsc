<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package  WellThemes
 * @file     tag.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>
<div id="content" class="post-archive tag-archive">
	<?php if ( have_posts() ) : ?>
		
		<header class="archive-header">
			<h2><?php printf( __( 'Tag Archives: %s', 'wellthemes' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h2>		
		</header>
		
		<?php
			$tag_description = tag_description();
			if (! empty( $tag_description )) {
				echo apply_filters( 'tag_archive_meta', '<div class="archive-desc section">' . $tag_description . '</div>' );
			}
		?>

		<div class="archive-postlist">
			<?php 
				while ( have_posts() ) : the_post();										
					get_template_part( 'content', 'excerpt' );
				endwhile; 
			?>
		</div>
		<?php wt_pagination(); ?>
	<?php endif; ?>
</div><!-- /content -->
<?php get_sidebar('left'); ?>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
