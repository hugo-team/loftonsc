<?php
/**
 * The template for displaying Author Archive pages.
 *
 * @package  WellThemes
 * @file     author.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>

<div id="content" class="post-archive author-archive">
	<?php if ( have_posts() ) : ?>
		<?php
			/* Queue the first post, that way we know
			 * what author we're dealing with (if that is the case).
			 *
			 * We reset this later so we can run the loop
			 * properly with a call to rewind_posts().
			 */
			the_post();
		?>

		<header class="archive-header">
			<h2>
				<?php printf( __( 'Archivos del Autor: %s', 'wellthemes' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?>
			</h2>
		</header>

		<?php
			/* Since we called the_post() above, we need to
			 * rewind the loop back to the beginning that way
			 * we can run the loop properly, in full.
			 */
			rewind_posts();
		?>

		<?php
			// If a user has filled out their description, show a bio on their entries.
			if ( get_the_author_meta( 'description' )) {?>
				<div class="archive-desc section">							
		
					<div class="author-avatar">
						<?php echo get_avatar( get_the_author_meta( 'user_email' ), 65 ); ?>
					</div>
					
					<div class="author-right">
						
						
						<p><?php the_author_meta( 'description' ); ?></p>
					</div>
					
					
						
				</div><!-- /entry-author -->
		<?php } ?>
		<div class="archive-postlist">
			<?php 
				while ( have_posts() ) : the_post();										
					get_template_part( 'content', 'excerpt' );
				endwhile; 
			?>
		</div>
		<?php wt_pagination(); ?>
	<?php endif; ?>
</div><!-- /content -->

<?php get_sidebar('left'); ?>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>