<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package  WellThemes
 * @file     search.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>
<div id="content" class="post-archive">
	<div class="archive">				
		<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h2 class="archive-title">
					<?php printf( __( 'Resultados de la busqueda para: %s', 'wellthemes' ), '<span>' . get_search_query() . '</span>' ); ?>
				</h2>
			</header>
	
			<div class="archive-postlist">
				<?php 
					while ( have_posts() ) : the_post();										
						get_template_part( 'content', 'excerpt' );
					endwhile; 
				?>
			</div>
			<?php wt_pagination(); ?>
		
		<?php else : ?>

			<article id="post-0" class="post no-results not-found">
				<header class="archive-header">
					<h3 class="archive-title">								
						<?php _e( 'Nada encontrado', 'wellthemes' ); ?>
					</h3>
				</header><!-- /entry-header -->

				<div class="entry-content">
					<p><?php _e( 'Lo sentimos, pero no encontramos nada de acuerdo a tu criterio. Por favor intenta de nuevo con algunas palabras diferentes.', 'wellthemes' ); ?></p>
					<div class="box-550">
						<?php get_search_form(); ?>
					</div>
				</div><!-- /entry-content -->
			</article><!-- /post-0 -->

		<?php endif; ?>
	</div><!-- /archive -->
</div><!-- /content -->
<?php get_sidebar('left'); ?>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>