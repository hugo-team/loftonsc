<?php
/**
 * The template for displaying the scrolling posts.
 * Gets the category for the posts from the theme options. 
 * If no category is selected, displays the latest posts.
 *
 * @package  WordPress
 * @file     ticker.php
 * @author   Wellthemes
 * @link 	 http://wellthemes.com
 */
?>

<div class="ticker-section">
	<script>
		jQuery(document).ready(function($) {
			$(".ticker").show();
			$(function() {
				$("#ticker-posts").simplyScroll();
			});
		});
	</script>
	
	<div class="inner-wrap">
		<div class="ticker">
			
			<div class="title">
				<i class="fa fa-angle-right"></i>
				<?php printf(__( '%sNoticias%s', 'wellthemes' ), '<span class="main-color">', '</span>' ); ?>
			</div>		
			
			<ul id="ticker-posts">
				<?php
					$cat_id = wt_get_option('wt_ticker_cat');
					
					$args = array(
						'cat' => $cat_id,
						'post_status' => 'publish',
						'ignore_sticky_posts' => 1,
						'posts_per_page' => 10
					);
				?>
				<?php $query = new WP_Query( $args ); ?>
					<?php if ( $query -> have_posts() ) : ?>
						<?php while ( $query -> have_posts() ) : $query -> the_post(); ?>
							<li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>							
						<?php endwhile; ?>
					<?php endif; ?>
				<?php wp_reset_query();?>
			</ul>

		</div>		
	</div>
</div><!-- /ticker-section -->