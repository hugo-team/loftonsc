<?php
$tags = wp_get_post_tags($post->ID);
$number = 3;
$args = array();
$args2 = array();
if ($tags) {
    $tag_ids = array();
    
	foreach($tags as $tag){
		$tag_ids[] = $tag->term_id;
	}

    $args = array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=> $number,
    ); 
	
    if( count($args) < $number ) {
        $n = $number - count($args);  //to get posts based on the category
        if ($categories) {
			$category_ids = array();
			foreach($categories as $cat) $category_ids[] = $cat->term_id;

			$args2 = array(     //this is the args array for category based posts
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'showposts'=> $n,
            );      
		}
    }
    $args = array_merge( $args, $args2 );
} else {
    $categories = get_the_category($post->ID);  
    if ($categories) {
        $category_ids = array();
        foreach($categories as $cat) $category_ids[] = $cat->term_id;

        $args = array(
            'category__in' => $category_ids,
            'post__not_in' => array($post->ID),
            'showposts'=> $number,
        );      
    }
}

if($args){
	$query = new WP_Query( $args ); ?>
	<?php if ( $query -> have_posts() ) : ?>
		<div class="related-posts" data-animation="fadeInUp">
			
			<h3><?php _e('Publicaciones Relacionadas', 'wellthemes'); ?></h3>			
			<div class="post-list">
				<?php $i = 0; ?>
				<?php while ( $query -> have_posts() ) : $query -> the_post(); ?>
					<?php								
						$post_class ="";
						if ( $i % 3 == 2 ){
							$post_class =" col-last";
						}					
					?>								
					<div class="one-third<?php echo $post_class; ?>">
						<?php if ( has_post_thumbnail() ) {	?>
							<div class="thumb">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt260_145' ); ?></a>															
							</div>
						<?php } ?>
								
						<div class="entry-meta">
							<?php wt_get_first_cat(); ?>						
							<span class="date"><?php echo get_the_date(); ?></span>	
																			
							<?php				
								$comment_count = get_comments_number($post->ID);
								if ($comment_count > 0){ ?>	
									<span class="comments">
										<i class="fa fa-comment"></i>
										<?php comments_popup_link( __('', 'wellthemes'), __( '1', 'wellthemes'), __('%', 'wellthemes')); ?>	
									</span>					
									<?php
								}
							?>					
						</div>
						<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
						<p>
							<?php 
								$excerpt = get_the_excerpt();
								$trimmed_excerpt = wp_trim_words( $excerpt, 15);
								echo $trimmed_excerpt;
							?>
						</p>
						<div class="excerpt-footer">
							<div class="more-link">
								<a href="<?php the_permalink() ?>"><?php _e('Leer mas', 'wellthemes'); ?></a>
							</div>
							<div class="share-links">
								<span class="share"><?php _e('Compartir', 'wellthemes'); ?></span>
								<span class="links">
									<a class="twitter" href="http://twitter.com/home?status=<?php echo urlencode( get_the_title() ); ?>%20<?php echo urlencode( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a>	
									<a  class="fb" href="http://facebook.com/share.php?u=<?php echo urlencode( get_the_permalink() ); ?>&amp;t=<?php echo urlencode( get_the_title() ); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
								</span>
							</div>
						</div>	
					</div>
					<?php $i++; ?>					
				<?php endwhile; ?>
			</div>
			
		</div>		
	<?php endif; ?>	
	<?php wp_reset_query();	
}

?>